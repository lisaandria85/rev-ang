// round.pipe.ts
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "round" })
export class RoundPipe implements PipeTransform {
  /**
   *
   * @param value
   * @returns {number}
   */
  transform(value: string): string {
    if (value && parseFloat(value) > 0) {
      return parseFloat(value).toFixed(1);
    }
    return '';
  }
}
