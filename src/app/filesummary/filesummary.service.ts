import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class FilesummaryService {
  private _filemannagementUrl = CONFIGURATION.basefileformsURL + 'file/';
  
  constructor (
    private http: Http
  ) {}
 
  getSummaryDetails(errId: number) {
    return this.http.get(this._filemannagementUrl + 'summary/' + errId)
    .map((res:Response) => res.json());
  }
}
