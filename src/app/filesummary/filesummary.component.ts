import { Component, OnInit } from '@angular/core';
import { FilesummaryService } from './filesummary.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-filesummary',
  templateUrl: './filesummary.component.html',
  styleUrls: ['./filesummary.component.css']
})
export class FilesummaryComponent implements OnInit {

  constructor(private userService: FilesummaryService, private route: ActivatedRoute) { }
  summaryDetails: any = {};
  errId: number;

  ngOnInit() {
    this.errId = parseInt(this.route.snapshot.params['errId']);
    this.userService.getSummaryDetails(this.errId).subscribe(data => this.summaryDetails = data);
    console.log(this.summaryDetails)
  }

}
