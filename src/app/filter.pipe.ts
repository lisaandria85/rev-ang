import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'columnPipe'
})
export class columnPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    let columnNames = [];
    for (var i = 0; i < value.length; i++) {
      for (let key in value[i]) {
        if (columnNames.indexOf(key) === -1) {
          columnNames.push(key);
        }
      }
    }
    return columnNames;
  }
} 

@Pipe({
  name: 'rowPipe'
})

export class rowPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    let rowValues = Object.keys(value);
    return rowValues.map(k => value[k]);
  }
}

@Pipe({
  name: 'searchPipe'
})
export class searchPipe implements PipeTransform {
  transform(value: any, args: string[]): any {
    let filter = args[0];

    if (filter && Array.isArray(value)) {
      let filterKeys = Object.keys(filter);
      return value.filter(item => filterKeys.reduce((key, keyName) => key && item[keyName].toUpperCase() === filter[keyName].toUpperCase(), true));
    }
    // new case !
    else if (filter && typeof value === 'string') {
      return value.toUpperCase().indexOf(filter.toUpperCase()) >= 0 ? value : '';
    }
    else {
      return value;
    }
  }
}
