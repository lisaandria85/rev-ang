import { TestBed, inject } from '@angular/core/testing';

import { PayhistoryService } from './payhistory.service';

describe('PayhistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PayhistoryService]
    });
  });

  it('should be created', inject([PayhistoryService], (service: PayhistoryService) => {
    expect(service).toBeTruthy();
  }));
});
