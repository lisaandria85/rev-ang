export interface payhistoryDetail {
    taxYear:string;
    controlGroup:string;


    Source:string;
    TaxYear:string;
    EmployeeSSN:string;
    EmployeeFirstName:string;
    EmployeeLastName:string;
    FirstWorkDate:string;
    PeriodStratDate:string;
    PeriodEndDate:string;
    LastWorkDate:string;
    UnionStatus:string;
    ShowName:string;
    ProductionCompany:string;
    HoursWorked:string;
    EmploymentStatus:string;
}