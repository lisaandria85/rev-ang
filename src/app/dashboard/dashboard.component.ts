import { Component, OnInit } from '@angular/core';
declare var Core: any;

@Component({
    moduleId: module.id,
    templateUrl: './dashboard.html'

})
export class DashboardComponent implements OnInit {

    constructor() {}

    ngOnInit(): void {
        Core.init();
    }
}
