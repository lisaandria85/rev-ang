import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { File1094Component } from './file1094.component';

describe('File1094Component', () => {
  let component: File1094Component;
  let fixture: ComponentFixture<File1094Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ File1094Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(File1094Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
