import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class file1094Service {
    private _filemannagementUrl = CONFIGURATION.basefile1094CURL;

    constructor(private http: Http) { }

    get1094yearlist() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        return this.http.get(this._filemannagementUrl + '1094c/tax-years', { headers: headers })
            .map((res: Response) => res.json());
    }
    get1094controlgroup(value: any) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        return this.http.get(this._filemannagementUrl + '1094c/control-groups?taxYear=' + value, { headers: headers })
            .map((res: Response) => res.json());
    }
    get1094einlist(value: any) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        return this.http.get(this._filemannagementUrl + '1094c/ein-list?controlGroupName=' + value, { headers: headers })
            .map((res: Response) => res.json());
    }
    getIRSeinlist(taxYear: string,  controlGroup: string, processStatus: string) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        return this.http.get(this._filemannagementUrl +
            `submission/ein-list?processStatus=${processStatus}&controlGroupName=${controlGroup}&taxYear=${taxYear}`,
            { headers: headers })
            .map((res: Response) => res.json());
    }

    getAll1094BySearchCriteria(taxYear: number, ein: string, controlGroup: string) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        if ('-1' === ein) ein = '';
        if ('-1' === controlGroup) controlGroup = '';
        return this.http.get(this._filemannagementUrl +
            `1094c/getAll1094BySearchCriteria?taxYear=${taxYear}&controlGroup=${controlGroup}&ein=${ein}`, { headers: headers })
            .map((res: Response) => res.json());
    }
}
