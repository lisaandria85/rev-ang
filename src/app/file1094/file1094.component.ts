import { Component, OnInit } from '@angular/core';
import { file1094Service } from './file1094.service';
import { SharedService } from '../app.shared.service';
import { Router } from '@angular/router';

import { AlertService } from '../alert/index';

import * as _ from 'lodash';

@Component({
  selector: 'app-file1094',
  templateUrl: './file1094.component.html',
  providers: [file1094Service],
  styleUrls: ['./file1094.component.css']
})
export class File1094Component implements OnInit {

  public einList: any[];
  public taxYearList: any[];
  public controlGroupList: any[];

  public file1094List: any[];
  public selectedYear: number = -1;
  public selectedEIN: string = '-1';
  public selectedGroup: string= '-1';
  public selected1094EIN: string;

  dataLoaded: boolean;
  public loading = false;
  public rows: Array<any> = [];
  public page: number = 1;
  public itemsPerPage: number = 50;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public columns: Array<any> = [
    { title: 'S.No', className: 'va-m', name: 'dataRecord1094Id' },
    { title: 'Age', className: 'va-m', name: 'p2TotNumOfForms1095cFiledBehalfofAlemember' },
    { title: 'EIN', className: 'va-m', name: 'p4EmployerIdentificationNumberEin1' }
  ];
  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table', 'table-striped', 'table-bordered', 'table-hover']
  };
  constructor(private file1094Serv: file1094Service, private sharedService: SharedService,
    private router: Router, private alertService: AlertService) { }

  ngOnInit() {
    this.file1094Serv.get1094yearlist().subscribe(data => {
      this.taxYearList = data.taxYears;
    });
  }
  ontaxYearChange() {
    if (this.selectedYear) {
      this.file1094Serv.get1094controlgroup(this.selectedYear).subscribe(data => {
        this.controlGroupList = data.controlGroups;
      });
    }
  }
  ongroupChange() {
    if (this.selectedGroup) {
      this.file1094Serv.get1094einlist(this.selectedGroup).subscribe(data => {
        this.einList = data.einList;
      });
    }
  }
  reset(): void {
    this.dataLoaded = false;
    this.alertService.clear();
    this.selectedYear = -1;
    this.selectedEIN = '-1';
    this.selectedGroup = '-1';
    // this.getListOfFiles()
  }
  getAll1094BySearchCriteria() {
    this.alertService.clear();
    this.loading = true;
    if (this.selectedYear == -1) {
      this.alertService.error('Please select a year');
      this.loading = false;
      return;
    }
    this.file1094Serv.getAll1094BySearchCriteria(this.selectedYear, this.selectedEIN, this.selectedGroup).subscribe(data => {
      this.file1094List = data;
      this.onChangeTable(this.config);
      this.dataLoaded = true;
      this.loading = false;
      console.log(this.file1094List);
    });

  }

  shareObj(data: any) {
    this.sharedService.setObj(data);
    this.router.navigate(['/reporting/details1094', this.selectedYear]);
  }
  public changePage(page: any, data: Array<any> = this.file1094List): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }


  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (_.toString(item[column.name]).match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }
  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.file1094List, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    /*this.rows.forEach(item => {
      item.p4EmployerIdentificationNumberEin1 = `<a (click)="shareObj(item)">${item.p4EmployerIdentificationNumberEin1}</a>`
    })*/

    this.length = sortedData.length;
  }
  onCellClick(event: any) {
   
    this.selected1094EIN = event.row;
    // this.updateUser.userName = event.row.backupUserName;
  }
}
