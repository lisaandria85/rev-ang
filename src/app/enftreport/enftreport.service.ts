import { Injectable } from '@angular/core';
import { Http, Response,Headers,ResponseContentType } from '@angular/http';
import { IWorkDetails } from './workdetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class ENFTReportService {
    private _enftreportUrl = CONFIGURATION.baseServiceUrl + 'newhiresnonfulltimereportservice/';
    private data: any;
    constructor(private _http: Http) {

    }

    getReportData(): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(this._enftreportUrl + 'getNewHiresNonFullTimeReportReferenceData',{headers:headers})
            .map((response: Response) => response.json().EligibilityNewHiresNonFullTimeReferenceData)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    getYears() { return this.data.WorkYear; }

    getMonths() { return this.data.WorkMonth; }

    getControlGroups() { return this.data.ControlGroup; }

    getTypeOfHours() { return this.data.UnionType; }

    getNonFullTimeCategories() { return this.data.EmployeeType; }

    getWeeklyCounts(filterCriteria: any): Observable<any> {
       
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName = 'getNewHiresNonFullTimeCountByWeek?WorkYear=' + filterCriteria.selectedYear
            + '&WorkMonth=' + filterCriteria.selectedHireMonth
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&UnionType=' + filterCriteria.selectedTypeOfHours
            + '&EmployeeType=' + filterCriteria.selectedNonFullTimeCatgeories
            + '&AvgWeeklyThreshold=' + filterCriteria.avgWeeklyThreshold ;
            //+ '';
        return this._http.get(this._enftreportUrl + fileName,{headers:headers})
            .map((response: Response) => response.json())
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
        // return { count13Weeks: "3", count26Weeks: "4", count47Weeks: "5", count52Weeks: "6" };
    }

    getWeekReportData(filterCriteria: any): Observable<IWorkDetails[]> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        const fileName = 'getNewHiresNonFullTimeReportData?WorkYear=' + filterCriteria.selectedYear
            + '&WorkMonth=' + filterCriteria.selectedHireMonth
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&UnionType=' + filterCriteria.selectedTypeOfHours
            + '&EmployeeType=' + filterCriteria.selectedNonFullTimeCatgeories
            + '&AvgWeeklyThreshold=' + filterCriteria.avgWeeklyThreshold
            + '&ReportOfWeek=' + filterCriteria.reportCount;
        return this._http.get(this._enftreportUrl + fileName,{headers:headers})
            .map((response: Response) => <IWorkDetails[]>response.json().reportByWeekCount)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    downloadPDFReport(filterCriteria: any): any {
        const fileName = 'processNewHiresNonFullTimeReportPDFUpload?WorkYear=' + filterCriteria.selectedYear
            + '&WorkMonth=' + filterCriteria.selectedHireMonth
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&UnionType=' + filterCriteria.selectedTypeOfHours
            + '&EmployeeType=' + filterCriteria.selectedNonFullTimeCatgeories
            + '&AvgWeeklyThreshold=' + filterCriteria.avgWeeklyThreshold
            + '&ReportOfWeek=' + filterCriteria.reportCount;

        //window.open(this._enftreportUrl + fileName, '_bank');
        const url = this._enftreportUrl+ fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        // headers.append('Authorization', 'JWT ' + localStorage.getItem('id_token'));
       return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }
    downloadExcelReport(filterCriteria: any): any {
        const fileName = 'processnewhiresnonfulltimeexcelzipdownload?WorkYear=' + filterCriteria.selectedYear
            + '&WorkMonth=' + filterCriteria.selectedHireMonth
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&UnionType=' + filterCriteria.selectedTypeOfHours
            + '&EmployeeType=' + filterCriteria.selectedNonFullTimeCatgeories
            + '&AvgWeeklyThreshold=' + filterCriteria.avgWeeklyThreshold
            + '&ReportOfWeek=' + filterCriteria.reportCount;

        //window.open(this._enftreportUrl + fileName, '_bank');
        const url = this._enftreportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }
    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
