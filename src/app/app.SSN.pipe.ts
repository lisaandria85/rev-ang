import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'SSN' })
export class SSNPipe implements PipeTransform {
    transform(value: string, args: string[]): any {
        if (!value) return value;
        
        return [value.slice(0,3),'-',value.slice(3,5),'-',value.slice(5,value.length)].join('');
        
        
        // value.toUpperCase();

        // return value.replace(/\w\S*/g, function (txt) {
        //     return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        // });
    }
}