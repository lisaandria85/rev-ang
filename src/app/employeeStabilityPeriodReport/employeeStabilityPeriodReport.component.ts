import { Component, OnInit,ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { employeeStabilityPeriodReportService } from './employeeStabilityPeriodReport.service'
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

@Component({
    selector: "app-employeeStabilityPeriodReport",
    templateUrl: './employeeStabilityPeriodReport.component.html',
    styleUrls: ['./employeeStabilityPeriodReport.component.css']
})
export class employeeStabilityPeriodReportComponent implements OnInit {
    topScrollWidth: number;


    errorMessage:string;
    selectedWorkYear:string;
    selectedControlGroup:string[];
    selectedEmpStatus:string[];
    workerDetails:Array<any>=[];
    downloadPdfInProgress:boolean=false;
    downloadExcelInProgress:boolean=false;
    



    dataLoaded:boolean;

    public loading = false;
    public rows: Array<any> = [];
    public page: number = 1;
    public itemsPerPage: number = 50;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;



    ControlGroup:Array<string>;
    EmployeeStatus:Array<string>;

    stabilityPeriodReportCount:string='0';


    optionsModel:number[];
    myOptions:IMultiSelectOption[];
    myEmpOptions:IMultiSelectOption[];
    mySettings:IMultiSelectSettings={
        enableSearch:false,
        showCheckAll:true,
        showUncheckAll:true,
        checkedStyle:"checkboxes",
        buttonClasses:"btn btn-default btn-block",
        dynamicTitleMaxItems:1,
        displayAllSelectedText:false
    };

    myEmpSettings:IMultiSelectSettings={
        enableSearch:false,
        showCheckAll:true,
        showUncheckAll:true,
        checkedStyle:'checkboxes',
        buttonClasses:'btn btn-default btn-block',
        dynamicTitleMaxItems:1,
        displayAllSelectedText:false
    };
    myTexts:IMultiSelectTexts={
        checkAll:'select all',
        uncheckAll:'unselect all'
    };
    myEmpTexts:IMultiSelectTexts={
        checkAll:'select all',
        uncheckAll:'unselect all'
    };

    @ViewChild('topScroll') topScroll: ElementRef;
    @ViewChild('bottomScroll') bottomScroll: ElementRef;

    public columns: Array<any> = [
        
                { title: 'IMP Stability Start Date', className: 'va-m', name: 'stabilityPeriodIMPStartDate' },
                { title: 'IMP Stability End DAte', classname: 'va-m', name: 'stabilityPeriodIMPEndDate' },
                { title: 'SMP Stability Start Date', className: 'va-m', name: 'stabilityPeriodSMPStartDate' },
                { title: 'SMP Stability End DAte', classname: 'va-m', name: 'stabilityPeriodSMPEndDate' },
                { title: 'Control Group Name', classNAme: 'va-m', name: 'controlGroup' },
                { title: 'Work Year', classNAme: 'va-m', name: 'workYear' },
                { title: 'Latest Production Company Name', className: 'va-m', name: 'latestProductionCompany' },
                { title: 'Most Recent Show', className: 'va-m', name: 'mostRecentShow' },
                { title: 'SSN Number', className: 'hidden-xs va-m', name: 'ssn' },
                { title: 'First Name', className: 'hidden-xs va-m', name: 'firstName' },
                { title: 'Last Name', className: 'va-m', name: 'lastName' },
                { title: 'Hire Date', className: 'va-m', name: 'hireDate' },
                { title: 'New Hire Date', className: 'va-m', name: 'newHireDate' },
                { title: 'Last Worked Date', className: 'va-m', name: 'lastWorkedDate' },
                { title: 'Employment Status', className: 'va-m', name: 'employementStatus' },
                { title: 'Union Type', className: 'va-m', name: 'unionType' },
                { title: 'IMP Start Date', className: 'va-m', name: 'impStartDate' },
                { title: 'IMP End Date', className: 'va-m', name: 'impEndDate' },
                { title: 'SMP Start Date', className: 'va-m', name: 'smpStartDate' },
                { title: 'SMP End Date', className: 'va-m', name: 'smpEndDate' },
                { title: 'Avg Hours IMP', className: 'va-m', name: 'avgHrsOfIMP' },
                { title: 'Total Hours IMP', className: 'va-m', name: 'totalHrsInIMP' },
                { title: 'Avg Hours SMP', className: 'va-m', name: 'avgHrsOfSMP' },
                { title: 'Total Hours SMP', className: 'va-m', name: 'totalHrsInSMP' }
            ];
public config:any={
    paging:true,
    sorting:{columns:this.columns},
    filtering:{filterString:''},
    className:['table','table-striped','table-bordered','table-hover']
};

    constructor(private _stabilityPeriodService : employeeStabilityPeriodReportService){}


    ngOnInit():void {
        this.myOptions=[];
        this.myEmpOptions=[];
        this._stabilityPeriodService.getReportData().subscribe(data=>{
            this.ControlGroup=data.controlGroupList;
            this.EmployeeStatus=data.employeeTypeList;
            data.controlGroupList.forEach(element=>{
                this.myOptions.push({id:element,name:element})
            });
            data.employeeTypeList.forEach(element=>{
                this.myEmpOptions.push({id:element,name:element})
            });
        },
        error=>this.errorMessage=<any>error
        );
        this.stabilityPeriodReportCount='0'
       
    }
    reset():void{
        this.selectedWorkYear='';
        this.selectedControlGroup=[];
        this.selectedEmpStatus=[];
        this.stabilityPeriodReportCount='0';
        this.dataLoaded = false;


    }
    stabilityPeriodReportData():void{
        this.loading=true;
        let filterCriteria=this.getFiltervalues();
        filterCriteria.satbilityPeriodReportCount=this.stabilityPeriodReportCount;
        this.dataLoaded = true;
        this._stabilityPeriodService.getStabilityPeriodReportData(filterCriteria).subscribe(workdetails=>{
            this.loading = false;
            this.workerDetails=workdetails;
            this.onChangeTable(this.config);
            this.topScrollWidth = this.bottomScroll.nativeElement.scrollWidth;
            //this.dataLoaded=true;
        },
        error=>this.errorMessage=<any>error);

    }






    getFiltervalues():any{
        let cg=this.selectedControlGroup;
        if(cg===undefined){
            cg=[];
            cg[0]='\'\'';
            this.selectedControlGroup=[];
            }
        if(cg!==undefined&&cg.length<=0){
            cg[0]='\'\'';
            this.selectedControlGroup=[];

        }
        let cgEmpStatus=this.selectedEmpStatus;
        if(cgEmpStatus===undefined){
            cgEmpStatus=[];
            cgEmpStatus[0]='\'\'';
            this.selectedEmpStatus=[];
        }
        if(cgEmpStatus!==undefined&&cgEmpStatus.length <=0 ){
            cgEmpStatus[0]='\'\'';
            this.selectedEmpStatus=[];

        }
        let cgWorkYear=this.selectedWorkYear
        if(cgWorkYear===undefined){
            cgWorkYear='\'\'';
            this.selectedWorkYear='\'\'';

        }
        if(cgWorkYear!==undefined && cgWorkYear.length<=0){
            cgWorkYear='\'\'';
            this.selectedWorkYear='\'\'';
        }
        const filterCriteria:any={
            selectedWorkYear:cgWorkYear,selectedEmpStatus:cgEmpStatus,selectedControlGroup:cg.join(':')
        }
        return filterCriteria;

    }
    Search():void{
        let filterCriteria=this.getFiltervalues();
        console.log(filterCriteria);
        this.stabilityPeriodReportCount='0'
        this._stabilityPeriodService.getStabilityPeriodReportCount(filterCriteria).subscribe(counts=>{
            if(counts===undefined||counts==null){
                return;
            }
            counts.forEach((element:any)=>{
                this.stabilityPeriodReportCount=element.satbilityPeriodReportCount;
            })
        })

    }

    downloadPdf(): void {
        this.downloadPdfInProgress=true;
        let filterCriteria = this.getFiltervalues();
        this._stabilityPeriodService.downloadPDFReport(filterCriteria).subscribe(data=>{this.downloadPdfInProgress=false
        },err=>(this.downloadPdfInProgress=false))
    }

    downloadExcel(): void {
        this.downloadExcelInProgress=true;
        let filterCriteria = this.getFiltervalues();
        this._stabilityPeriodService.downloadExcelReport(filterCriteria).subscribe(data=>{this.downloadExcelInProgress=false
        },err=>(this.downloadExcelInProgress=false))
    }






    public changePage(page: any, data: Array<any> = this.workerDetails): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }


    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name].toLowerCase().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }
    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.workerDetails, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }


    arescrolling = 0;
    scroller(from: any, to: any) {
        if (this.arescrolling) return; // avoid potential recursion/inefficiency
        this.arescrolling = 1;
        // set the other div's scroll position equal to ours
        // this.el;
        // debugger;
        from.scrollLeft = to.scrollLeft;
        // document.getElementById(to).scrollLeft =
        //     document.getElementById(from).scrollLeft;
        this.arescrolling = 0;
    }




    
    
}