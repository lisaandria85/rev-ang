import { Injectable } from '@angular/core';
import { Http, Response ,Headers,ResponseContentType} from '@angular/http';
import{ IEmployeeStabilityPeriodReportDetail} from './employeeStabilityPeriodReportDetail'
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class employeeStabilityPeriodReportService {
    private _stabilityPeriodUrl=CONFIGURATION.baseServiceUrl+"stabilityperiodreport/";
    constructor(private _http:Http){}
    getReportData():Observable<any>{
        const headers=new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(this._stabilityPeriodUrl+'reference-data',{headers:headers})
        .map((response:Response)=>response.json())
        .do(data=>console.log('All'+JSON.stringify(data)))
        .catch(this.handleError);

    }
    getStabilityPeriodReportCount(filterCriteria:any):Observable<any>{
        const headers=new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName:string='stability-count?WorkYear='+filterCriteria.selectedWorkYear
        +'&ControlGroup='+filterCriteria.selectedControlGroup
        +'&EmployeeStatus='+filterCriteria.selectedEmpStatus;
       return this._http.get(this._stabilityPeriodUrl+fileName,{headers:headers})
        .map((response:Response)=>response.json())
        .do(data=>console.log('All'+JSON.stringify(data)))
        .catch(this.handleError);


    }
    getStabilityPeriodReportData(filterCriteria:any):Observable<IEmployeeStabilityPeriodReportDetail[]>{
        const headers=new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName:string='stability-data?WorkYear='+filterCriteria.selectedWorkYear
        +'&ControlGroup='+filterCriteria.selectedControlGroup
        +'&EmployeeStatus='+filterCriteria.selectedEmpStatus;
       return this._http.get(this._stabilityPeriodUrl+fileName,{headers:headers})
       .map((response:Response)=><IEmployeeStabilityPeriodReportDetail[]>response.json())
       .do(data=>console.log('All'+JSON.stringify(data)))
       .catch(this.handleError);



    }

    downloadPDFReport(filterCriteria: any): any {
        let fileName = "pdf-download?WorkYear=" + filterCriteria.selectedWorkYear
            + "&ControlGroup=" + filterCriteria.selectedControlGroup
            + "&EmployeeStatus=" + filterCriteria.selectedEmpStatus;

        //window.open(this._nhftreportUrl + fileName, '_bank');
        const url = this._stabilityPeriodUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
       
       return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }


    downloadExcelReport(filterCriteria: any): any {
        let fileName = "excel-download?WorkYear=" + filterCriteria.selectedWorkYear
            + "&ControlGroup=" + filterCriteria.selectedControlGroup
            + "&EmployeeStatus=" + filterCriteria.selectedEmpStatus;

       
        const url = this._stabilityPeriodUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }
    





    private handleError(error:Response){
        console.log(error);
        return Observable.throw(error.json().error||"failed in web api(server error)");
    }

   
}













 