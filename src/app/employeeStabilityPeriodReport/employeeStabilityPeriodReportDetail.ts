export interface IEmployeeStabilityPeriodReportDetail{
    WorkYear:string;
    Controlgroup:string;
    EmployeeStatus:string;

    stabilityPeriodIMPStartDate:string;
    stabilityPeriodIMPEndDate:string;
    stabilityPeriodSMPStartDate:string;
    stabilityPeriodSMPEndDate:string;
    controlGroup:string;
    workYear:string;
    latestProductionCompany:string;
    mostRecentShow:string;
    ssn:string;
    firstName:string;
    lastName:string;
    hireDate:string;
    newHireDate:string;
    lastWorkedDate :string;
    employementStatus:string;
    unionType:string;
    impStartDate:string;
    impEndDate:string;
    smpStartDate:string;
    smpEndDate:string;
    avgHrsOfIMP:string;
    totalHrsInIMP:string;
    avgHrsOfSMP:string;
    totalHrsInSMP:string;
 
 }