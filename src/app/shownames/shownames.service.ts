import { Injectable } from '@angular/core';
import { Http, Response, Headers, ResponseContentType } from '@angular/http';
import { ShownamesDetail } from './shownamesDetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class ShownamesService {
    private _shownamesServiceReportUrl = CONFIGURATION.baseServiceUrl + 'shownamereport/';
    constructor(private _http: Http) { }

    getReportData(): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(this._shownamesServiceReportUrl + 'reference-data', { headers: headers })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getShownameServiceReports(filterCriteria: any): Observable<ShownamesDetail[]> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        const fileName = 'showname-data?WorkYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
        return this._http.get(this._shownamesServiceReportUrl + fileName, { headers: headers })
            .map((response: Response) => <ShownamesDetail[]>response.json())
            .catch(this.handleError);
    }

    downloadExcelReport(filterCriteria: any): any {
        const fileName = 'excel-download?WorkYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
        const url = this._shownamesServiceReportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })

    }
    // downloadPDFReport(filterCriteria: any): any {
    //     let fileName = 'processBreakInReportPDFUpload?WorkYear=' + filterCriteria.selectedYear
    //         + '&ControlGroup=' + filterCriteria.selectedControlGroup;
    //     const url = this._shownamesServiceReportUrl + fileName
    //     const headers = new Headers();
    //     headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
    //     headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
    //     // headers.append('Authorization', 'JWT ' + localStorage.getItem('id_token'));
    //     return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
    //         (res) => {
    //             const a = document.createElement('a');
    //             document.body.appendChild(a);
    //             const blob = new Blob([res.blob()], { type: 'application/zip' });
    //             const downloadUrl = URL.createObjectURL(blob);
    //             window.open(downloadUrl);
    //         })
    // }

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }
}
