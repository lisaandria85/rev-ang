import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { ShownamesService } from './shownames.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';



@Component({
    selector: 'app-shownames',
    templateUrl: './shownames.component.html',
    styleUrls: ['./shownames.component.css']
})
export class ShownamesComponent implements OnInit {
    topScrollWidth: number;
    dataLoaded: boolean;
    
    selectedYear: string[];
    selectedControlGroup: string[];
    selectedWeekStarting: string;
    selectedWeekEnding: string;

    downloadPdfInProgress: boolean = false;
    downloadExcelInProgress: boolean = false;

    workYearList: Array<string>;
    controlGroupList: Array<string>;
    //public loading = false;
    public rows: Array<any> = [];
    public page: number = 1;
    public itemsPerPage: number = 25;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;

    optionsModel: number[];
    myOptionsYear: IMultiSelectOption[];
    myOptionsCG: IMultiSelectOption[];

    mySettings: IMultiSelectSettings = {
        enableSearch: false,
        showCheckAll: true,
        showUncheckAll: true,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default btn-block',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false
    };

    // Text configuration
    myTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all'
    };

    @ViewChild('topScroll') topScroll: ElementRef;
    @ViewChild('bottomScroll') bottomScroll: ElementRef;

    public columns: Array<any> = [
        { title: 'Tax Year', className: 'va-m', name: 'taxYear' },
        { title: 'Controlled Group', className: 'va-m', name: 'controlGroup' },
        { title: 'Employer EIN', className: 'va-m', name: 'emoployerEIN' },
        { title: 'Employer Name', className: 'va-m', name: 'employerName' },
        { title: 'Show Name', className: 'va-m', name: 'showName' },
        { title: 'Source', className: 'va-m', name: 'source' }
       
    ];

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table', 'table-striped', 'table-bordered', 'table-hover']
    };

    employeeBeakInService: Array<any> = [];
    errorMessage: string;
    constructor(private _shownamesservice: ShownamesService) { }

    ngOnInit(): void {

        this.myOptionsYear = [];
        this.myOptionsCG = [];

        this._shownamesservice.getReportData().subscribe(data => {
           
            data.workYearList.forEach(element => {
               
                this.myOptionsYear.push({ id: element, name: element })
            });

            data.controlGroupList.forEach(element => {
                this.myOptionsCG.push({ id: element, name: element })
            });
            // this.selectedWeekStarting = data.WeekStarting;
            // this.selectedWeekEnding = data.WeekEnding;
        },
            error => this.errorMessage = <any>error);
        // if (this.selectedWeekStarting === '') {
        //     this.selectedWeekStarting = '2016-12-30';
        // }
        // if (this.selectedWeekEnding === '') {
        //     this.selectedWeekEnding = '2017-12-30';
        // }

    }
    reset(): void {
        this.selectedYear = [];
        this.selectedControlGroup = [];
        this.dataLoaded = false;
    }
    getFilterValues(): any {
       // this.loading = true;
        let year = this.selectedYear;
        if (year === undefined) {
            year = [];
            year[0] = '\'\'';
            this.selectedYear = [];
        }
        if (year !== undefined && year.length <= 0) {
            year[0] = '\'\'';
            this.selectedYear = [];
        }

        let cg = this.selectedControlGroup;
        if (cg === undefined) {
            cg = [];
            cg[0] = '\'\'';
            this.selectedControlGroup = [];
        }
        if (cg !== undefined && cg.length <= 0) {
            cg[0] = '\'\'';
            this.selectedControlGroup = [];
        }

        // let ws = this.selectedWeekStarting;
        // if (ws === '' || ws === undefined) {
        //     ws = "''";
        // }
        // let we = this.selectedWeekEnding;
        // if (we === '' || we === undefined) {
        //     we = "''";
        // }
        let filterCriteria: any = {
            selectedYear: year.join(':'),
            selectedControlGroup: cg.join(':'),
            // selectedWeekStart: ws,
            // selectedWeekEnding: we
        };

        return filterCriteria;
    }
    Search(): void {
        // this.onChangeTable(this.config);
        this.dataLoaded = true;

        this.employeeBreakInServiceReports();
    }

    employeeBreakInServiceReports(): void {
        const filterCriteria = this.getFilterValues();

        this._shownamesservice.getShownameServiceReports(filterCriteria).subscribe(empbreakinservice => {
            this.employeeBeakInService = empbreakinservice;
            this.onChangeTable(this.config);
            this.topScrollWidth = this.bottomScroll.nativeElement.scrollWidth;

            // this.loading = false;
        },
            error => this.errorMessage = <any>error);

    }
    // downloadPdf(): void {
    //     this.downloadPdfInProgress = true;
    //     const filterCriteria = this.getFilterValues();
    //     this._shownamesservice.downloadPDFReport(filterCriteria).subscribe(data => {
    //         this.downloadPdfInProgress = false;
    //     }, err => (this.downloadPdfInProgress = false));
    // }

    downloadExcel(): void {
        this.downloadExcelInProgress = true;
        let filterCriteria = this.getFilterValues();
        this._shownamesservice.downloadExcelReport(filterCriteria).subscribe(data => {
            this.downloadExcelInProgress = false
        }, err => (this.downloadExcelInProgress = false));
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        const columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        const filteredData = this.changeFilter(this.employeeBeakInService, this.config);
        const sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }
    arescrolling = 0;
    scroller(from: any, to: any) {
        if (this.arescrolling) return; // avoid potential recursion/inefficiency
        this.arescrolling = 1;
        // set the other div's scroll position equal to ours
        // this.el;
        // debugger;
        from.scrollLeft = to.scrollLeft;
        // document.getElementById(to).scrollLeft =
        //     document.getElementById(from).scrollLeft;
        this.arescrolling = 0;
    }

    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        const tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name] == null) {
                    item[column.name] = '';
                }
            });
            this.columns.forEach((column: any) => {
                if (item[column.name].toLowerCase().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public changePage(page: any, data: Array<any> = this.employeeBeakInService): Array<any> {
        const start = (page.page - 1) * page.itemsPerPage;
        const end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

}
