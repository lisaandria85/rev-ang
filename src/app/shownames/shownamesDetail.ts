export interface ShownamesDetail {
    source:string;
    controlGroup:string;
    emoployerEIN:string;
    employerName:string;
    showName:string;
    taxYear:string; 
    controlGroupList:string;
    workYearList:string;
}
