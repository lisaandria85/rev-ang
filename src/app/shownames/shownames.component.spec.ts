import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShownamesComponent } from './shownames.component';

describe('ShownamesComponent', () => {
  let component: ShownamesComponent;
  let fixture: ComponentFixture<ShownamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShownamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShownamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
