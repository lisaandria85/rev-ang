import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Detail1095Component } from './detail1095.component';

describe('Detail1095Component', () => {
  let component: Detail1095Component;
  let fixture: ComponentFixture<Detail1095Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Detail1095Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Detail1095Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
