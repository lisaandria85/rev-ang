import { Component, OnInit } from '@angular/core';
import { SharedService } from '../app.shared.service';
import { Router } from '@angular/router';

import { detail1095Service } from './detail1095.service'
import { AlertService } from '../alert/index';
import { RoundPipe } from '../round.pipe';


@Component({
  selector: 'app-detail1095',
  templateUrl: './detail1095.component.html',
  providers: [detail1095Service],
  styleUrls: ['./detail1095.component.css']
})
export class Detail1095Component implements OnInit {

  public details: any;
  downloading1095Progress = false;
  updatingInProgress = false;

  p3DependentFirstName: any = '';
  p3DependentSsn: any = '';
  p3DependentDateOfBirth: any = '';
  p3CoveredAll12Months: boolean;
  p3MonthsOfCoverageJan: boolean;
  p3MonthsOfCoverageFeb: boolean;
  p3MonthsOfCoverageMar: boolean;
  p3MonthsOfCoverageApr: boolean;
  p3MonthsOfCoverageMay: boolean;
  p3MonthsOfCoverageJun: boolean;
  p3MonthsOfCoverageJul: boolean;
  p3MonthsOfCoverageAug: boolean;
  p3MonthsOfCoverageSep: boolean;
  p3MonthsOfCoverageOct: boolean;
  p3MonthsOfCoverageNov: boolean;
  p3MonthsOfCoverageDec: boolean;

  constructor(private detail1095Service: detail1095Service, private sharedService: SharedService,
              private router: Router, private alertService: AlertService) { }
              onModelChange(event,key){
                this.details[key]=event.target.value;
              }
  ngOnInit() {
    this.details = this.sharedService.getObj();
    if (this.details && this.details.dependents.length < 17) {
      const dependentsR = 18 - this.details.dependents.length;
      for ( let i = 0; i < dependentsR; i++) {
        this.details.dependents.push({
          p3DependentFirstName: this.p3DependentFirstName, p3DependentSsn: this.p3DependentSsn,
          p3DependentDateOfBirth: this.p3DependentDateOfBirth,  p3CoveredAll12Months: 0,
          p3MonthsOfCoverageJan: 0, p3MonthsOfCoverageFeb: 0, p3MonthsOfCoverageMar: 0,
          p3MonthsOfCoverageApr: 0, p3MonthsOfCoverageMay: 0, p3MonthsOfCoverageJun: 0,
          p3MonthsOfCoverageJul: 0,  p3MonthsOfCoverageAug: 0, p3MonthsOfCoverageSep: 0,
          p3MonthsOfCoverageOct: 0, p3MonthsOfCoverageNov: 0, p3MonthsOfCoverageDec: 0
        })
      }
    }
    if (!this.details) {
      this.router.navigate(['/reporting/1095']);
    }
    console.log(this.details);
  }
  public viewPdf(id: number): any {
    this.downloading1095Progress = true;
    this.detail1095Service.get1095PDF(id)
      .subscribe(
        result => {
          var blobUri = URL.createObjectURL(result);
          window.open(blobUri);
          this.downloading1095Progress = false;
        },
        (error) => this.downloading1095Progress = false
      );
  }
  public save1095C(data: any): any {
        this.alertService.clear();
    this.updatingInProgress = true;
    this.detail1095Service.update1095(data).subscribe(result => {
          this.alertService.clear();
          this.updatingInProgress = false;
          this.alertService.success('Updated completed successful');
          console.log(this.details);
          // console.log('Successful', result);
        }, (error) => {
          // console.log('Error', error);
          this.alertService.error('Updated did not complete');
          this.updatingInProgress = false;
        });
  }

  p3IsSelfInsuredCoverage(event: any) {
    this.details.p3IsSelfInsuredCoverage = event.target.checked ? '1' : '0';
  }

  p3CoveredAll12MonthsF(event: any, i: any) {
    this.details.dependents[i].p3CoveredAll12Months = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3MonthsOfCoverageJan = '0';
    this.details.dependents[i].p3MonthsOfCoverageFeb = '0';
    this.details.dependents[i].p3MonthsOfCoverageMar = '0';
    this.details.dependents[i].p3MonthsOfCoverageApr = '0';
    this.details.dependents[i].p3MonthsOfCoverageMay = '0';
    this.details.dependents[i].p3MonthsOfCoverageJun = '0';
    this.details.dependents[i].p3MonthsOfCoverageJul = '0';
    this.details.dependents[i].p3MonthsOfCoverageAug = '0';
    this.details.dependents[i].p3MonthsOfCoverageSep = '0';
    this.details.dependents[i].p3MonthsOfCoverageOct = '0';
    this.details.dependents[i].p3MonthsOfCoverageNov = '0';
    this.details.dependents[i].p3MonthsOfCoverageDec = '0';
  }

  p3MonthsOfCoverageJanF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageJan = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageFebF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageFeb = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageMarF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageMar = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageAprF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageApr = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageMayF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageMay = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageJunF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageJun = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageJulF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageJul = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageAugF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageAug = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageSepF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageSep = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageOctF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageOct = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageNovF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageNov = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }
  p3MonthsOfCoverageDecF(event: any, i: any) {
    this.details.dependents[i].p3MonthsOfCoverageDec = event.target.checked ? '1' : '0';
    this.details.dependents[i].p3CoveredAll12Months = '0';
  }

}
