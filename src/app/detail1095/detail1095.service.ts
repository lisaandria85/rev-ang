import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';


@Injectable()
export class detail1095Service {
    private _1094Url = CONFIGURATION.basefile1094CURL;
    constructor(private http: Http) { }

  get1095PDF(id: number) {
    const headers = new Headers();
    headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
    headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
    return this.http.get(this._1094Url + `1095c/generatePdfById?id=${id}`, {headers: headers, responseType: ResponseContentType.Blob})
      .map((res: Response) => {
        return new Blob([res.blob()], { type: 'application/pdf' })
      });
  }
  update1095(data: any) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
    headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
    const content = JSON.stringify(data);
    return this.http.put(this._1094Url + '1095c/update1095ByEINAndSSN', content,  {headers: headers})
    .map((res: Response) => res.json());
  }
}
