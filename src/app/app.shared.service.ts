export class SharedService {
    private obj: any;

    setObj(data: any) {
        this.obj = data;
    }

    getObj() {
        return this.obj;
    }
}