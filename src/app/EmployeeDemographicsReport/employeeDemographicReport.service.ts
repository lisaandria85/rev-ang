import { Injectable } from '@angular/core';
import { Http, Response ,Headers,ResponseContentType} from '@angular/http';
import { IEmployeeDemographicDetail } from './employeeDemographicDetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class EmployeeDemographicReportService {
    private _empDemographicsReportUrl = CONFIGURATION.baseServiceUrl + 'demographicsreportservice/';
    constructor(private _http: Http) { }
    getReportData(): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(this._empDemographicsReportUrl + 'getDemographicsReferenceData',{headers:headers})
            .map((response: Response) => response.json().demoGraphicsReferanceData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getEmployeeDemographicsReports(filterCriteria: any): Observable<IEmployeeDemographicDetail[]> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName = 'getDemographicsReportData?WorkYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&ParentCompnay=' + filterCriteria.selectedParentCompany
            + '&Proudctioncompany=' + filterCriteria.selectedProductionCompany
            + '&PayrollCompany=' + filterCriteria.selectedPayrollCompany;
        return this._http.get(this._empDemographicsReportUrl + fileName,{headers:headers})
            .map((response: Response) => <IEmployeeDemographicDetail[]>response.json().demoGraphicsReportData)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    downloadPDFReport(filterCriteria: any): any {
        let fileName = 'processDemographicsReportPDFUpload?WorkYear='
            + filterCriteria.selectedYear + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&ParentCompnay=' + filterCriteria.selectedParentCompany
            + '&Proudctioncompany=' + filterCriteria.selectedProductionCompany
            + '&PayrollCompany=' + filterCriteria.selectedPayrollCompany;

       // window.open(this._empDemographicsReportUrl + fileName, '_bank');

       const url = this._empDemographicsReportUrl + fileName
       const headers = new Headers();
       headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
       headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
       // headers.append('Authorization', 'JWT ' + localStorage.getItem('id_token'));
      return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
           (res) => {
               const a = document.createElement('a');
               document.body.appendChild(a);
               const blob = new Blob([res.blob()], { type: 'application/zip' });
               const downloadUrl = URL.createObjectURL(blob);
               window.open(downloadUrl);
           })
    }

    downloadExcelReport(filterCriteria: any): any {
        let fileName = 'processDemographicsServiceReportExcelUpload?WorkYear='
            + filterCriteria.selectedYear + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&ParentCompnay=' + filterCriteria.selectedParentCompany
            + '&Proudctioncompany=' + filterCriteria.selectedProductionCompany
            + '&PayrollCompany=' + filterCriteria.selectedPayrollCompany;

        //window.open(this._empDemographicsReportUrl + fileName, '_bank');
        const url = this._empDemographicsReportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }
}
