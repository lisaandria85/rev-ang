import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { EmployeeDemographicReportService } from './employeeDemographicReport.service';

@Component({
    moduleId: module.id,
    templateUrl: 'employeeDemographicReport.html'

})
export class EmployeeDemographicReportComponent implements OnInit {
    // ngAfterViewInit(): void {
    //    this.topScrollWidth = this.bottomScroll.nativeElement.scrollWidth;
    // }
    dataLoaded: boolean;
    topScrollWidth: number;
    selectedYear: string;
    selectedControlGroup: string;
    selectedParentCompany: string;
    selectedProductionCompany: string;
    selectedPayrollCompany: string;
    downloadPdfInProgress: boolean = false;
    downloadExcelInProgress: boolean = false;

    ParentCompanies: Array<string>;
    ProductionCompanies: Array<string>;
    PayrollCompanies: Array<string>;
    Years: Array<string>;
    ControlGroups: Array<string>;

    public loading = false;
    public rows: Array<any> = [];
    public page = 1;
    public itemsPerPage = 10;
    public maxSize = 5;
    public numPages = 1;
    public length = 0;
    @ViewChild('topScroll') topScroll: ElementRef;
    @ViewChild('bottomScroll') bottomScroll: ElementRef;


    public columns: Array<any> = [
        { title: 'Controlled Group', className: 'va-m', name: 'ControlGroup' },
        // { title: 'Work Year', className: 'va-m', name: 'WorkYear' },
        { title: 'Parent Company', className: 'va-m', name: 'ParentCompany' },
        { title: 'Prodcution Company', className: 'va-m', name: 'ProductionCompany' },
        { title: 'Show Name', className: 'va-m', name: 'ShowName' },
        { title: 'Pay Roll Company', className: 'va-m', name: 'PayrollCompany' },
        { title: 'Employee Name', className: 'va-m', name: 'EmployeeName' },
        { title: 'Union Status', className: 'va-m', name: 'UnionStatus' },
        { title: 'SSN', className: 'va-m', name: 'SSN' },
        { title: 'ACA Employment Basis', className: 'va-m', name: 'ACAEmploymentBasis' },
        { title: 'Last Work Date', class: 'va-m', name: 'LastWorkedDate' },
        { title: 'Schedule Code', className: 'va-m', name: 'ScheduleCode' },
        { title: 'Pay Rate', className: 'va-m', name: 'PayRate' },
        { title: 'Job Description', className: 'va-m', name: 'JobDescription' },
        { title: 'Gender', className: 'va-m', name: 'Gender' },
        { title: 'Date of Birth', className: 'va-m', name: 'DateOfBirth' },
        { title: 'Email', className: 'va-m', name: 'Email' },
        { title: 'Address', className: 'va-m', name: 'Address' },
        { title: 'City', className: 'va-m', name: 'City' },
        { title: 'State', className: 'va-m', name: 'State' },
        { title: 'Zip', className: 'va-m', name: 'Zip' },
        { title: 'Country', className: 'va-m', name: 'Country' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table', 'table-striped', 'table-bordered', 'table-hover']
    };

    employeeDemographicDetails: Array<any> = [];
    errorMessage: string;
    constructor(private _employeeDemographicReportService: EmployeeDemographicReportService) { }

    ngOnInit(): void {

        this._employeeDemographicReportService.getReportData().subscribe(data => {
            this.Years = data.WorkYear;
            this.ControlGroups = data.ControlGroup;
            this.ParentCompanies = data.ParentCompany;
            this.ProductionCompanies = data.ProductionCompany;
            this.PayrollCompanies = data.PayrollCompany;

        },
            error => this.errorMessage = <any>error);
        this.onChangeTable(this.config);
        this.reset();

        this.selectedYear = '-1';
        this.selectedControlGroup = '-1';
        this.selectedParentCompany = '-1';
        this.selectedProductionCompany = '-1';
        this.selectedPayrollCompany = '-1';
    }
    reset(): void {
        this.selectedYear = '-1';
        this.selectedControlGroup = '-1';
        this.dataLoaded = false;
    }
    getFilterValues(): any {
       
        let year = this.selectedYear;
        if (year === '-1') {
            year = "''";
        }

        let cg = this.selectedControlGroup;
        if (cg === 'All' || cg === '-1' || cg === undefined) {
            cg = "''";
        }
        let parentComp = this.selectedParentCompany;
        if (parentComp === '' || parentComp === '-1' || parentComp === undefined) {
            parentComp = "''";
        }
        let prodComp = this.selectedProductionCompany;
        if (prodComp === '' || prodComp === '-1' || prodComp === undefined) {
            prodComp = "''";
        }
        let payrollComp = this.selectedPayrollCompany;
        if (payrollComp === '' || payrollComp === '-1' || payrollComp === undefined) {
            payrollComp = "''";
        }
        const filterCriteria: any = {
            selectedYear: year,
            selectedControlGroup: cg,
            selectedParentCompany: parentComp,
            selectedProductionCompany: prodComp,
            selectedPayrollCompany: payrollComp
        };

        return filterCriteria;
    }
    Search(): void {
        // this.onChangeTable(this.config);
        this.dataLoaded = true;

        this.employeeDemographicsReports();
    }

    employeeDemographicsReports(): void {
        this.loading = true;
        const filterCriteria = this.getFilterValues();
        this._employeeDemographicReportService.getEmployeeDemographicsReports(filterCriteria).subscribe(empDemographics => {
           
            this.loading = false;
            this.employeeDemographicDetails = empDemographics;
            this.onChangeTable(this.config);
            this.topScrollWidth = this.bottomScroll.nativeElement.scrollWidth;

        },
            error => this.errorMessage = <any>error);

    }
    downloadPdf(): void {
        this.downloadPdfInProgress = true;
        const filterCriteria = this.getFilterValues();
        this._employeeDemographicReportService.downloadPDFReport(filterCriteria).subscribe(data => {
        this.downloadPdfInProgress = false;
        }, err => (this.downloadPdfInProgress = false));
    }

    downloadExcel(): void {
        this.downloadExcelInProgress = true;
        const filterCriteria = this.getFilterValues();
        this._employeeDemographicReportService.downloadExcelReport(filterCriteria).subscribe(data => {
        this.downloadExcelInProgress = false
        }, err => (this.downloadExcelInProgress = false));
    }
    public onCellClick(data: any): any {
        console.log(data);
        alert();
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        const columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        const filteredData = this.changeFilter(this.employeeDemographicDetails, this.config);
        const sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }
    arescrolling = 0;
    scroller(from: any, to: any) {
        if (this.arescrolling) return; // avoid potential recursion/inefficiency
        this.arescrolling = 1;
        // set the other div's scroll position equal to ours
        // this.el;
        // debugger;
        from.scrollLeft = to.scrollLeft;
        // document.getElementById(to).scrollLeft =
        //     document.getElementById(from).scrollLeft;
        this.arescrolling = 0;
    }



    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        const tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name] == null) {
                    item[column.name] = '';
                }
            });
            this.columns.forEach((column: any) => {
                if (item[column.name].toLowerCase().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public changePage(page: any, data: Array<any> = this.employeeDemographicDetails): Array<any> {
        const start = (page.page - 1) * page.itemsPerPage;
        const end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

}
