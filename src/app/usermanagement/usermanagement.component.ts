import { Component, OnInit } from '@angular/core';
import { UsermanagementService } from './usermanagement.service';
import { CONFIGURATION } from '../app.config';

import * as _ from "lodash";

import { AlertService } from '../alert/index';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.css']
})
export class UsermanagementComponent {
  route: string = CONFIGURATION.reportingroute;
  addcustomer: string = CONFIGURATION.addcustomer;
  pwdStrengthbarLabel: string = 'Password Strength:';

  employeelist = {};
  addUser: any = {
    role:''
  }
  updateUser: any = {};
  public empDataList: Array<any> = [];
  errorMessage: String;

  dataLoaded: boolean;
  public rows: Array<any> = [];
  public page: number = 1;
  public itemsPerPage: number = 50;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;
  updateUserNewPassword: string = '';
  public loading = false;

  public columns: Array<any> = [
    { title: 'First Name', className: 'va-m', name: 'firstName' },
    { title: 'Last Name', className: 'va-m', name: 'lastName' },
    { title: 'User Name', className: 'va-m', name: 'username' },
    { title: 'Active', className: 'va-m', name: 'enabled' },
    { title: 'Modified By', className: 'va-m', name: 'modifiedBy' },
    { title: 'Created By', className: 'va-m', name: 'createdBy' }
  ];
  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table', 'table-striped', 'table-bordered', 'table-hover']
  };
  constructor(private userService: UsermanagementService, private alertService: AlertService) { }
  // tslint:disable-next-line:use-life-cycle-interface
  getEmployees(){
    this.loading = true;
    this.userService.getEmployeelist()
      .subscribe(data => {
          this.empDataList = data;
          console.log(this.empDataList);
          this.onChangeTable(this.config);
          this.dataLoaded = true;
          this.loading = false;
        },
        error => {
          this.loading = false;
          this.alertService.error('Error Loading Employee List');
        });
  }
  ngOnInit() {
    this.getEmployees();
  }
  public changePage(page: any, data: Array<any> = this.empDataList): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }


  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (_.toString(item[column.name]).match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }
  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.empDataList, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.rows.forEach(item => {
      item.backupUsername = item.username;
      item.username = `<a data-toggle="modal" data-target="#myModalupdate">${item.username}</a>`
    })

    this.length = sortedData.length;
  }

  onCellClick(event: any) {
  
    this.updateUser = event.row;
    // this.updateUser.userName = event.row.backupUserName;
  }


  sucessMassage: any;
  adduserFunction() {
    //console.log(JSON.stringify(this.addUser));
    this.loading = true;
    console.log(this.addUser)
    this.userService.addUser(this.addUser)
      .subscribe(data => {
        //alert(JSON.stringify(data));
        // this.sucessMassage = 'User added Sucessfully';
        this.loading = false;
       
        this.alertService.success('User added successfully');
      }, error => {
        this.loading = false;
        this.alertService.error('User not added');
      });
    this.resetFunction();
  }

  updateuserFunction() {
    //console.log(JSON.stringify(this.addUser));
    this.loading = true;
    this.updateUser.username = this.updateUser.backupUsername;
    console.log(this.updateUser)
    this.userService.updateUser(this.updateUser)
    
      .subscribe(data => {
        //alert(JSON.stringify(data));

        this.loading = false;
        this.alertService.success('User added successfully');
      }, error => {
        this.loading = false;
        this.alertService.error('User not updated');
      });
    this.resetFunction();
  }
  resetFunction() {
    this.addUser.firstName = '';
    this.addUser.lastName = '';
    this.addUser.userName = '';
    this.addUser.emailid = '';
    this.addUser.password = '';
    this.addUser.role = 'adassd';
  }
  clearAlerts(){
    this.alertService.clear();
  }
}
