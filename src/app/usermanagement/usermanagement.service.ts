import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
// import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class UsermanagementService {
  private _LoginUrl = CONFIGURATION.baseLoginURL;
  constructor(
    private http: Http
  ) { }

  getEmployeelist() {
    const headers = new Headers();
    headers.append('Client-Id', 'HIC-USER-MANAGEMENT-API-CLIENT');
    headers.append('Client-Secret', '44c2f7a0-b554-4c47-aa72-5f09280b521d');
    return this.http.get(this._LoginUrl + 'all', { headers: headers })
      .map((res: Response) => res.json())
      // .do(data => console.log('All: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }
  private handleError(error: Response) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    console.error(error);
    return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
  }

  addUser(user: any) {
    let headers = new Headers();
    headers.append('Client-Id', 'HIC-USER-MANAGEMENT-API-CLIENT');
    headers.append('Client-Secret', '44c2f7a0-b554-4c47-aa72-5f09280b521d');
    headers.append('Content-Type', 'application/json');
    let data = JSON.stringify(user);
    return this.http.post(this._LoginUrl + 'save', data, { headers: headers })
      .map((response: Response) => {
        return response.json();
      });
  }

  updateUser(user: any) {
    let headers = new Headers();
    headers.append('Client-Id', 'HIC-USER-MANAGEMENT-API-CLIENT');
    headers.append('Client-Secret', '44c2f7a0-b554-4c47-aa72-5f09280b521d');
    headers.append('Content-Type', 'application/json');
    // let data = JSON.stringify(user);
    debugger
    return this.http.put(this._LoginUrl + 'update/' + user.employeeId, user, { headers: headers })
      .map((response: Response) => {
        return response.json();
      });
  }
}
