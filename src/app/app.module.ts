import './rxjs-extension';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { Ng2TableModule } from 'ng2-table';
import { PaginationModule, TabsModule } from 'ng2-bootstrap';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { APPROUTES } from './app.routes';
import { ExportToExcelService } from './shared/index';
import { ClientOnboardingCustomerInfoService } from './OnBoardingCustomerInformation/index';
import { LoginService } from './login/index';
import { FilemanagerService } from './filemanager/index';
import { UsermanagementService } from './usermanagement/index';
import { MatButtonModule, MatCheckboxModule, MatRadioModule, MatSelectModule } from '@angular/material';
import { NgUploaderModule } from 'ngx-uploader';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { MyDatePickerModule } from 'mydatepicker';
import { FilesummaryService } from './filesummary/filesummary.service';
import { FileerrorsService } from './fileerrors/fileerrors.service';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';
import { LoadingModule } from 'ngx-loading';

import {
  SideNavComponent, TopNavComponent, ENFTReportComponent, ENFTReportService, OnGoingReportComponent,
  PayrollDataActivityReportComponent, PayrollDataActivityReportService,
  ErCoverageReportComponent, ErCoverageReportService,
  EmployeeEligibilityReportComponent, EmployeeEligibilityReportService,
  EmployeeDemographicReportComponent, EmployeeDemographicReportService,
  EmployeeBreakInServiceReportComponent, EmployeeBreakInServiceReportService,
  ListCustomerComponent,
  OnboardingCustomerInformationComponent,
  OnboardingPersonalInformationComponent,
  ControlGroupComponent, ApplicableLargeEmployeeComponent,
  ApplicableLargeEmployeeDetailsComponent,
  ClientPayrollComponent,
  AleDataUploadComponent,
  InsuranceDataUploadComponent,
  OneZeroNineFourDataUploadComponent,
  OneZeroNineFiveDataUploadComponent,
  PayrollDataUploadComponent,
  EmployeeSummaryReportComponent, EmployeeSummaryReportService,
  ControlGroupService, ApplicableLargeEmployeeService, ApplicableLargeEmployeeDetailsService,
  OnGoingReportService, NewHireFullTimeComponent, NewHireFullTimeService, DashboardComponent, LoginComponent,
  LegalEntitiesReportComponent, LegalEntitiesReportService,
  InsuranceInfoReportComponent, InsuranceInfoReportService,
  AcaDataReportComponent, AcaDataReportService,
  Original1095cReportComponent, Original1095cService,
  Detail1094Component, Detail1095Component
} from './index';
import { FilemanagerComponent } from './filemanager/filemanager.component';
import { AuthGuard } from './login/auth-guard.service';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { FilesummaryComponent } from './filesummary/filesummary.component';
import { FileerrorsComponent } from './fileerrors/fileerrors.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { File1094Component } from './file1094/file1094.component';
import { File1095Component } from './file1095/file1095.component';
import { IrsFileManagementComponent } from './irs-file-management/irs-file-management.component';
import { SSNPipe } from './app.SSN.pipe';
import { EINPipe } from './app.EIN.pipe';
import { SharedService } from './app.shared.service';
import { GenerateIRSFileComponent } from './generate-irsfile/generate-irsfile.component';
import { AlertComponent } from './alert/alert.component';
import { AlertService } from './alert/index';
import { RoundPipe } from './round.pipe';
import { columnPipe, rowPipe, searchPipe } from './filter.pipe';
import { employeeStabilityPeriodReportComponent } from './employeeStabilityPeriodReport/employeeStabilityPeriodReport.component';
import { employeeStabilityPeriodReportService } from "./employeeStabilityPeriodReport/employeeStabilityPeriodReport.service";
import { ShownamesComponent } from './shownames/shownames.component';
import { ShownamesService } from './shownames/shownames.service';
import { PayhistoryComponent } from './payhistory/payhistory.component';
import {PayhistoryService } from './payhistory/payhistory.service'
// import {FilemanagerService} from "./filemanager/filemanager.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MatButtonModule, MatCheckboxModule,
    MatRadioModule, MatSelectModule,
    MultiselectDropdownModule,
    RouterModule.forRoot(APPROUTES, { useHash: true }),
    Ng2TableModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    NgUploaderModule,
    AngularMultiSelectModule,
    MyDatePickerModule,
    PasswordStrengthBarModule,
    LoadingModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    SideNavComponent,
    TopNavComponent,
    ENFTReportComponent,
    OnGoingReportComponent,
    NewHireFullTimeComponent,
    PayrollDataActivityReportComponent,
    ListCustomerComponent,
    OnboardingCustomerInformationComponent,
    ClientPayrollComponent,
    OnboardingPersonalInformationComponent,
    AleDataUploadComponent,
    ControlGroupComponent,
    ApplicableLargeEmployeeComponent,
    ApplicableLargeEmployeeDetailsComponent,
    ErCoverageReportComponent,
    EmployeeEligibilityReportComponent,
    EmployeeDemographicReportComponent,
    EmployeeBreakInServiceReportComponent,
    EmployeeSummaryReportComponent,
    InsuranceDataUploadComponent,
    OneZeroNineFourDataUploadComponent,
    OneZeroNineFiveDataUploadComponent,
    PayrollDataUploadComponent,
    LoginComponent,
    LegalEntitiesReportComponent,
    InsuranceInfoReportComponent,
    AcaDataReportComponent,
    Original1095cReportComponent,
    FilemanagerComponent,
    UnauthorizedComponent,
    FilesummaryComponent,
    FileerrorsComponent,
    UsermanagementComponent,
    File1094Component,
    Detail1094Component,
    Detail1095Component,
    File1095Component,
    IrsFileManagementComponent,
    SSNPipe,
    EINPipe,
    GenerateIRSFileComponent,
    AlertComponent,
    RoundPipe,
    columnPipe,
    rowPipe,
    searchPipe,
    employeeStabilityPeriodReportComponent,
    ShownamesComponent,
    PayhistoryComponent
  ],
  providers: [
    ENFTReportService,
    OnGoingReportService,
    ExportToExcelService,
    NewHireFullTimeService,
    ErCoverageReportService,
    EmployeeEligibilityReportService,
    EmployeeDemographicReportService,
    EmployeeBreakInServiceReportService,
    PayrollDataActivityReportService,
    EmployeeSummaryReportService,
    ControlGroupService,
    ApplicableLargeEmployeeService,
    ApplicableLargeEmployeeDetailsService,
    employeeStabilityPeriodReportService,
    ClientOnboardingCustomerInfoService,
    LegalEntitiesReportService,
    InsuranceInfoReportService,
    AcaDataReportService,
    Original1095cService,
    LoginService,
    FilemanagerService,
    UsermanagementService,
    AuthGuard,
    FilesummaryService,
    FileerrorsService,
    SharedService,
    AlertService,
    ShownamesService,
    PayhistoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

