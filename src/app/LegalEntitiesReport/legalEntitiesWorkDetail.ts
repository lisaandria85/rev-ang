export interface ILegalEntitiesWorkDetail {
    TaxYear: string;
    ControlGroup: string;
    EmployerEIN: string;
    EmployerName: string;
    AddressLine1: string;
    AddressLine2: string;
    City: string;
    State: string;
    ZipCode: String;
    ContactFirstName: string;
    ContactLastName: string;
    ContactPhone: string;
    TransitionRelief: string;
    IsAuthoritative: string;
    UnionRuleType: string;
    NonUnionRuleType: string;
}
