import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ILegalEntitiesWorkDetail } from './legalEntitiesWorkdetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';


@Injectable()
export class LegalEntitiesReportService {
    private _legalEntitiesReportUrl = CONFIGURATION.baseServiceUrl + 'legalentitiesreportservice/';
    constructor(private _http: Http) { }
    getReportReferenceData(): Observable<any> {
        return this._http.get(this._legalEntitiesReportUrl + 'getlegalentitiesreferencedata')
            .map((response: Response) => response.json().legalEntitiesReferanceData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getLegalEntitiesReports(filterCriteria: any): Observable<ILegalEntitiesWorkDetail[]> {
        let fileName = 'getlegalentitiesreportdata?TaxYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
            return this._http.get(this._legalEntitiesReportUrl + fileName)
            .map((response: Response) => <ILegalEntitiesWorkDetail[]>response.json().legalEntitiesReportData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    downloadExcelReport(filterCriteria: any): void {
        let fileName = 'processlegalentitiesserviceexcelupload?TaxYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
        window.open(this._legalEntitiesReportUrl + fileName, '_bank');
    }
    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }
}
