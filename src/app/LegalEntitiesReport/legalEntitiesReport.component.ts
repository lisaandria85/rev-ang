import { Component, OnInit } from '@angular/core';
import { LegalEntitiesReportService } from './legalEntitiesReport.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

@Component({
    moduleId: module.id,
    templateUrl: 'legalEntitiesReport.html'

})
export class LegalEntitiesReportComponent implements OnInit {

    dataLoaded: boolean;
    selectedYear: string[];
    selectedControlGroup: string[];
    Years: Array<string>;
    ControlGroups: Array<string>;

    public rows: Array<any> = [];
    public page = 1;
    public itemsPerPage = 10;
    public maxSize = 5;
    public numPages = 1;
    public length = 0;

    optionsModel: number[];
    myOptionsYear: IMultiSelectOption[];
    myOptionsCG: IMultiSelectOption[];

    mySettings: IMultiSelectSettings = {
        enableSearch: false,
        showCheckAll: true,
        showUncheckAll: true,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default btn-block',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false
    };

    // Text configuration
    myTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all'
    };

    public columns: Array<any> = [
        { title: 'Control Group', className: 'va-m', name: 'ControlGroup' },
        { title: 'Tax Year', className: 'va-m', name: 'TaxYear' },
        { title: 'EIN', className: 'va-m', name: 'EmployerEIN' },
        { title: 'Name', className: 'va-m', name: 'EmployerName' },
        { title: 'Address1', className: 'va-m', name: 'AddressLine1' },
        { title: 'Address2', className: 'va-m', name: 'AddressLine2' },
        { title: 'City', className: 'va-m', name: 'City' },
        { title: 'State', className: 'va-m', name: 'State' },
        { title: 'Zip Code', className: 'va-m', name: 'ZipCode' },
        { title: 'Contact First Name', className: 'va-m', name: 'ContactFirstName' },
        { title: 'Contact Last Name', className: 'va-m', name: 'ContactLastName' },
        { title: 'Contact Phone', className: 'va-m', name: 'ContactPhone' },
        { title: 'Transition Relief', className: 'va-m', name: 'TransitionRelief' },
        { title: 'Authoritative', className: 'va-m', name: 'IsAuthoritative' },
        { title: 'Union Rule Type', className: 'va-m', name: 'UnionRuleType' },
        { title: 'Non Union Rule Type', className: 'va-m', name: 'NonUnionRuleType' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table', 'table-striped', 'table-bordered', 'table-hover']
    };

    empDetails: Array<any> = [];
    errorMessage: string;
    constructor(private _legalEntitiesReportService: LegalEntitiesReportService) { }

    ngOnInit(): void {
        this.myOptionsYear = [];
        this.myOptionsCG = [];
        this._legalEntitiesReportService.getReportReferenceData().subscribe(data => {

            data.TaxYear.forEach(element => {
                this.myOptionsYear.push({ id: element, name: element })
            });

            data.ControlGroup.forEach(element => {
                this.myOptionsCG.push({ id: element, name: element })
            });
        },
            error => this.errorMessage = <any>error);
    }


    reset(): void {
        this.selectedYear = [];
        this.selectedControlGroup = [];
        this.dataLoaded = false;
    }

    getFilterValues(): any {

        let tyear = this.selectedYear;
        if (tyear === undefined) {
            tyear = [];
            tyear[0] = '\'\'';
            this.selectedYear = [];
        }
        if (tyear !== undefined && tyear.length <= 0) {
            tyear[0] = '\'\'';
            this.selectedYear = [];
        }

        let cg = this.selectedControlGroup;
        if (cg === undefined) {
            cg = [];
            cg[0] = '\'\'';
            this.selectedControlGroup = [];
        }
        if (cg !== undefined && cg.length <= 0) {
            cg[0] = '\'\'';
            this.selectedControlGroup = [];
        }
        let filterCriteria: any = {
            selectedYear: tyear.join(':'),
            selectedControlGroup: cg.join(':')
        };

        return filterCriteria;
    }

    legalEntitiesReportsData(): void {
        
        const filterCriteria = this.getFilterValues();
        this._legalEntitiesReportService.getLegalEntitiesReports(filterCriteria).subscribe(empdetails => {
            
            this.empDetails = empdetails;
            this.onChangeTable(this.config);
            this.dataLoaded = true;
        },
            error => this.errorMessage = <any>error);

    }

    Search(): void {
        this.dataLoaded = false;
        this.legalEntitiesReportsData();
    }

    downloadPdf(): void {
    }

    downloadExcel(): void {
        const filterCriteria = this.getFilterValues();
        this._legalEntitiesReportService.downloadExcelReport(filterCriteria);
    }
    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        const columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        const filteredData = this.changeFilter(this.empDetails, this.config);
        const sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }

    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        const tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name].toString().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public changePage(page: any, data: Array<any> = this.empDetails): Array<any> {
        const start = (page.page - 1) * page.itemsPerPage;
        const end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

}
