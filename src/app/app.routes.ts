import { Routes } from '@angular/router';
import {
    AppComponent, ENFTReportComponent, OnGoingReportComponent,
    NewHireFullTimeComponent, PayrollDataActivityReportComponent,
    DashboardComponent, LoginComponent, TopNavComponent,
    ErCoverageReportComponent,
    EmployeeEligibilityReportComponent,
    EmployeeDemographicReportComponent,
    EmployeeBreakInServiceReportComponent,
    ControlGroupComponent,
    ApplicableLargeEmployeeComponent,
    ApplicableLargeEmployeeDetailsComponent,
    ListCustomerComponent,
    OnboardingCustomerInformationComponent,
    OnboardingPersonalInformationComponent,
    ClientPayrollComponent,
    EmployeeSummaryReportComponent,
    AleDataUploadComponent,
    InsuranceDataUploadComponent,
    OneZeroNineFourDataUploadComponent,
    OneZeroNineFiveDataUploadComponent,
    PayrollDataUploadComponent,
    LegalEntitiesReportComponent,
    InsuranceInfoReportComponent,
    AcaDataReportComponent,
    Original1095cReportComponent,
    SideNavComponent,
    Detail1094Component,
    Detail1095Component

} from './index';
import { FilemanagerComponent } from './filemanager/filemanager.component';
import { FilesummaryComponent } from './filesummary/filesummary.component';
import { FileerrorsComponent } from './fileerrors/fileerrors.component';
import { AuthGuard } from './login/auth-guard.service';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { File1094Component } from './file1094/file1094.component';
import { File1095Component } from './file1095/file1095.component';
import { IrsFileManagementComponent } from './irs-file-management/irs-file-management.component';
import { GenerateIRSFileComponent } from "./generate-irsfile/generate-irsfile.component";
import { employeeStabilityPeriodReportComponent } from './employeeStabilityPeriodReport/employeeStabilityPeriodReport.component'
import { ShownamesComponent } from './shownames/shownames.component';
import { PayhistoryComponent } from './payhistory/payhistory.component';



export const APPROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'unauthorized', component: UnauthorizedComponent },
    { path: 'reporting', component: AppComponent },
    {
        path: 'reporting', children: [
            { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
            { path: 'usermanagement', component: UsermanagementComponent, canActivate: [AuthGuard] },
            { path: '1094', component: File1094Component, canActivate: [AuthGuard] },
            { path: '1095', component: File1095Component, canActivate: [AuthGuard] },
            { path: 'details1094/:year', component: Detail1094Component, canActivate: [AuthGuard] },
            { path: 'details1095', component: Detail1095Component, canActivate: [AuthGuard] },
            { path: 'irsfilemanagement', component: IrsFileManagementComponent, canActivate: [AuthGuard] },
            { path: 'generateirs', component: GenerateIRSFileComponent, canActivate: [AuthGuard] },
            { path: 'filemanager', component: FilemanagerComponent, canActivate: [AuthGuard] },
            { path: 'filesummary/:errId', component: FilesummaryComponent, canActivate: [AuthGuard] },
            { path: 'fileerrors/:errId', component: FileerrorsComponent, canActivate: [AuthGuard] },
            { path: 'enftreport', component: ENFTReportComponent, canActivate: [AuthGuard] },
            { path: 'ogreport', component: OnGoingReportComponent, canActivate: [AuthGuard] },
            { path: 'nhftreport', component: NewHireFullTimeComponent, canActivate: [AuthGuard] },
            { path: 'pdareport', component: PayrollDataActivityReportComponent, canActivate: [AuthGuard] },
            { path: 'ercreport', component: ErCoverageReportComponent, canActivate: [AuthGuard] },
            { path: 'empeligilbility', component: EmployeeEligibilityReportComponent, canActivate: [AuthGuard] },
            { path: 'empstabilityperiod', component: employeeStabilityPeriodReportComponent, canActivate: [AuthGuard] },
            { path: 'empdemographics', component: EmployeeDemographicReportComponent, canActivate: [AuthGuard] },
            { path: 'empbreakinservice', component: EmployeeBreakInServiceReportComponent, canActivate: [AuthGuard] },
            { path: 'shownames', component: ShownamesComponent, canActivate: [AuthGuard] },
            {path: 'payhistory',component:PayhistoryComponent,canActivate:[AuthGuard] },
            { path: 'listcustomer', component: ListCustomerComponent, canActivate: [AuthGuard] },
            { path: 'onboardingcustomerinformation', component: OnboardingCustomerInformationComponent, canActivate: [AuthGuard] },
            { path: 'clientpayroll', component: ClientPayrollComponent, canActivate: [AuthGuard] },
            { path: 'onboardingpersonalinformation', component: OnboardingPersonalInformationComponent, canActivate: [AuthGuard] },
            { path: 'uploaddata', component: AleDataUploadComponent, canActivate: [AuthGuard] },
            { path: 'empsummary', component: EmployeeSummaryReportComponent, canActivate: [AuthGuard] },
            { path: 'controlgroup', component: ControlGroupComponent, canActivate: [AuthGuard] },
            { path: 'ale', component: ApplicableLargeEmployeeComponent, canActivate: [AuthGuard] },
            { path: 'aledetails', component: ApplicableLargeEmployeeDetailsComponent, canActivate: [AuthGuard] },
            { path: 'aledataupload', component: AleDataUploadComponent, canActivate: [AuthGuard] },
            { path: 'payrolldataupload', component: PayrollDataUploadComponent, canActivate: [AuthGuard] },
            { path: 'onezeroninefourdataupload', component: OneZeroNineFourDataUploadComponent, canActivate: [AuthGuard] },
            { path: 'onezeroninefivedataupload', component: OneZeroNineFiveDataUploadComponent, canActivate: [AuthGuard] },
            { path: 'insurancedataupload', component: InsuranceDataUploadComponent, canActivate: [AuthGuard] },
            { path: 'legalentities', component: LegalEntitiesReportComponent, canActivate: [AuthGuard] },
            { path: 'insuranceinfo', component: InsuranceInfoReportComponent, canActivate: [AuthGuard] },
            { path: 'acadata', component: AcaDataReportComponent, canActivate: [AuthGuard] },
            { path: 'o1095c', component: Original1095cReportComponent, },
            { path: '', component: TopNavComponent, outlet: 'header' },
            { path: '', component: SideNavComponent, outlet: 'sidebar' },
        ]
    },
    { path: '', redirectTo: '/login', pathMatch: 'full' }
];
