import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { IInsuranceInfoWorkDetail } from './insuranceInfoWorkDetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';


@Injectable()
export class InsuranceInfoReportService {
    private _legalEntitiesReportUrl = CONFIGURATION.baseServiceUrl + 'insurancereportservice/';
    constructor(private _http: Http) { }
    getReportReferenceData(): Observable<any> {
        return this._http.get(this._legalEntitiesReportUrl + 'getinsurancereferencedata')
            .map((response: Response) => response.json().insuranceReferanceData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getInsuranceInfoReports(filterCriteria: any): Observable<IInsuranceInfoWorkDetail[]> {
        let fileName = 'getinsurancereportdata?TaxYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
           // + '&TypeOfHours=' + filterCriteria.selectedTypeOfHours
            //+ '&UnionStatus=' + filterCriteria.selectedUnionStatus;
        return this._http.get(this._legalEntitiesReportUrl + fileName)
            .map((response: Response) => <IInsuranceInfoWorkDetail[]>response.json().insuranceReportData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    downloadExcelReport(filterCriteria: any): void {
        let fileName = 'processinsuranceserviceexcelupload?TaxYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
            //+ '&TypeOfHours=' + filterCriteria.selectedTypeOfHours
            //+ '&UnionStatus=' + filterCriteria.selectedUnionStatus;

        window.open(this._legalEntitiesReportUrl + fileName, '_bank');
    }
    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }
}
