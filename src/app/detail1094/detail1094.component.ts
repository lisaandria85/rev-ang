import { Component, OnInit, OnDestroy } from "@angular/core";
import { SharedService } from "../app.shared.service";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import * as FileSaver from "file-saver";

import { AlertService } from "../alert/index";

import { detail1094Service } from "./detail1094.service";

@Component({
  selector: "app-detail1094",
  providers: [detail1094Service],
  templateUrl: "./detail1094.component.html",
  styleUrls: ["./detail1094.component.css"]
})
export class Detail1094Component implements OnInit, OnDestroy {
  public details: any;
  public year: number;
  public sub: any;
  updatingInProgress: boolean = false;
  download1094InProgress: boolean = false;
  download1095InProgress: boolean = false;

  

  constructor(
    private detail1094Serv: detail1094Service,
    private sharedService: SharedService,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.details = this.sharedService.getObj();
    if (!this.details) {
      this.router.navigate(["/reporting/1094"]);
    }
    this.sub = this.activateRoute.params.subscribe(params => {
      this.year = +params["year"]; // (+) converts string 'id' to a number
      // console.log('Year', this.year);
      // In a real app: dispatch action to load the details here.
    });
  }

  public viewPdf(year: number, ein: string): any {
    this.download1094InProgress = true;
    this.detail1094Serv.get1094PDF(year.toString(), ein).subscribe(result => {
      var blobUri = URL.createObjectURL(result);
      window.open(blobUri);
      this.download1094InProgress = false;
    }, error => (this.download1094InProgress = false));
  }

  public view1095sPdf(year: number, ein: string): any {
    this.download1095InProgress = true;
    this.detail1094Serv
      .getAll1095PDF(year.toString(), ein)
      .subscribe(result => {
        var blobUri = URL.createObjectURL(result);
        window.open(blobUri);
        this.download1095InProgress = false;
      }, error => (this.download1095InProgress = false));
  }

  public save1094C(data: any): any {
    this.updatingInProgress = true;
    this.alertService.clear();
    this.detail1094Serv.update1094(data).subscribe(result => {
      console.log(data);
        console.log("Successful", result);
        this.alertService.clear();
        this.alertService.success("Updated completed successful");
        this.updatingInProgress = false;
      },
      error => {
        this.alertService.error("Updated did not complete");
        this.updatingInProgress = false;
      }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // Reserved123
  reservedToggle(event: any) {
    this.details.p1Resever = event.target.checked ? '1' : '0';
  }
  // Is this the authoritative transmittal
  AuthTransmittal(event: any) {
    this.details.p1AuthTransmittalForThisAleMember = event.target.checked ? '1' : '0';
  }
  // Is ALE Member a member of an Aggregated ALE Group?
  updateALEGroup(value: string) {
    this.details.p2IsAleMemberAMemberOfAnAggregatedAleGroup = value;
  }
  updateALEgroup(value: string) {
    this.details.p2IsAleMemberAMemberOfAnAggregatedAleGroup = value;
  }

  // Certifications of Eligibility (select all that apply):
  qualifyingOfferMethod(event: any) {
    this.details.p2CertOfEligibilityAQualifyingOfferMethod = event.target.checked ? '1' : '0';
  }
  QualifyingOfferMethodReserved(event: any) {
    this.details.p2CertOfEligibilityBQualifyingOfferMethodReserved = event.target.checked ? '1' : '0';
  }
  Section4980hTransitionRelief(event: any) {
    this.details.p2CertOfEligibilityCSection4980hTransitionRelief = event.target.checked ? '1' : '0';
  }
  EligibilityD98OfferMethod(event: any) {
    this.details.p2CertOfEligibilityD98OfferMethod = event.target.checked ? '1' : '0';
  }


  alemembermonth12(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = value;
    this.details.p3MinimumEssentialCoverageOfferIndicatorJan = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorFeb = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorMar = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorApr = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorMay = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorJun = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorJul = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorAug = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorSep = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorOct = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorNov = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorDec = '';
  }
  p3AggregatedGroupIndicatorAll(event: any) {
    this.details.p3AggregatedGroupIndicatorAll = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorJan =  '0';
    this.details.p3AggregatedGroupIndicatorFeb =  '0';
    this.details.p3AggregatedGroupIndicatorMar =  '0';
    this.details.p3AggregatedGroupIndicatorApr =  '0';
    this.details.p3AggregatedGroupIndicatorMay =  '0';
    this.details.p3AggregatedGroupIndicatorJun =  '0';
    this.details.p3AggregatedGroupIndicatorJul =  '0';
    this.details.p3AggregatedGroupIndicatorAug =  '0';
    this.details.p3AggregatedGroupIndicatorSep =  '0';
    this.details.p3AggregatedGroupIndicatorOct =  '0';
    this.details.p3AggregatedGroupIndicatorNov =  '0';
    this.details.p3AggregatedGroupIndicatorDec =  '0';
  }
  alemembermonthjan(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorJan = value;
  }
  p3AggregatedGroupIndicatorJan(event: any) {
    this.details.p3AggregatedGroupIndicatorJan = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthfeb(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorFeb = value;
  }
  p3AggregatedGroupIndicatorFeb(event: any) {
    this.details.p3AggregatedGroupIndicatorFeb = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }

  alemembermonthmar(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorMar = value;
  }
  p3AggregatedGroupIndicatorMar(event: any) {
    this.details.p3AggregatedGroupIndicatorMar = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }

  alemembermonthapr(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorApr = value;
  }
  p3AggregatedGroupIndicatorApr(event: any) {
    this.details.p3AggregatedGroupIndicatorApr = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }

  alemembermonthmay(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorMay = value;
  }
  p3AggregatedGroupIndicatorMay(event: any) {
    this.details.p3AggregatedGroupIndicatorMay = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthjun(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorJun = value;
  }
  p3AggregatedGroupIndicatorJun(event: any) {
    this.details.p3AggregatedGroupIndicatorJun = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthjul(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorJul = value;
  }
  p3AggregatedGroupIndicatorJul(event: any) {
    this.details.p3AggregatedGroupIndicatorJul = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthaug(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorAug = value;
  }
  p3AggregatedGroupIndicatorAug(event: any) {
    this.details.p3AggregatedGroupIndicatorAug = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthsep(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorSep = value;
  }
  p3AggregatedGroupIndicatorSep(event: any) {
    this.details.p3AggregatedGroupIndicatorSep = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthoct(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = ''; 
    this.details.p3MinimumEssentialCoverageOfferIndicatorOct = value;
  }
  p3AggregatedGroupIndicatorOct(event: any) {
    this.details.p3AggregatedGroupIndicatorOct = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthnov(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorNov = value;
  }
  p3AggregatedGroupIndicatorNov(event: any) {
    this.details.p3AggregatedGroupIndicatorNov = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
  alemembermonthdec(value: string) {
    this.details.p3MinimumEssentialCoverageOfferIndicatorAll = '';
    this.details.p3MinimumEssentialCoverageOfferIndicatorDec = value;
  }
  p3AggregatedGroupIndicatorDec(event: any) {
    this.details.p3AggregatedGroupIndicatorDec = event.target.checked ? '1' : '0';
    this.details.p3AggregatedGroupIndicatorAll = '0';
  }
}
