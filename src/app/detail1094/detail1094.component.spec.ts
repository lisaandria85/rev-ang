import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Detail1094Component } from './detail1094.component';

describe('Detail1094Component', () => {
  let component: Detail1094Component;
  let fixture: ComponentFixture<Detail1094Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Detail1094Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Detail1094Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
