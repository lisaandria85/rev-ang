import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class detail1094Service {
  private _1094Url = CONFIGURATION.basefile1094CURL;

  constructor(private http: Http) {}

// Generate 1094 Pdf
  get1094PDF(year: string, ein: string) {
    const headers = new Headers();
    headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
    headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');

    const params: URLSearchParams = new URLSearchParams();
    params.set('year', year);
    params.set('ein', ein);
    return this.http.get(this._1094Url + `1094c/generatePdf?taxYear=${year}&ein=${ein}`, {
        search: params, responseType: ResponseContentType.Blob, headers: headers
      })
      .map((res: Response) => {
        return new Blob([res.blob()], { type: 'application/pdf' });
      });
  }

  // Generate 1095 Pdf
  getAll1095PDF(year: string, ein: string) {
    const headers = new Headers();
    headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
    headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');

    const params: URLSearchParams = new URLSearchParams();
    params.set('year', year);
    params.set('ein', ein);
    return this.http.get(this._1094Url + `1095c/generatePdf?taxYear=${year}&ein=${ein}`,
      {headers: headers, search: params, responseType: ResponseContentType.Blob})
      .map((res: Response) => {
        return new Blob([res.blob()], { type: 'application/pdf' });
      });
  }

// Save 1094 Pdf
  update1094(data: any) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
    headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
    const content = JSON.stringify(data);
    return this.http
      .post(this._1094Url + '1094c/update1094ByEIN', content, {headers: headers})
      .map((res: Response) => res.json());
  }
}
