import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
// import { IErCoverageWorkDetail } from './erCoverageWorkDetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';


@Injectable()
export class FilemanagerService {

    // private _erCoverageReportUrl = 'app/api/';
    private _filemannagementUrl = CONFIGURATION.basefileformsURL + 'file/';

    constructor(private _http: Http) { }

    getAllFiles(): Observable<any> {
        return this._http.get(this._filemannagementUrl + 'all')
            .map((response: Response) => response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getFilesWithFilters(filterCriteria: any): Observable<any> {
        let fileName: string = 'all?dataType=' + filterCriteria.selectedDateType + '&startDate=' + filterCriteria.selectedStartDate + '&endDate=' + filterCriteria.selectedEndDate;
        return this._http.get(this._filemannagementUrl + fileName)
            .map((response: Response) => response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    fileChange(event: any, fileDataType:any): Observable<any> {
        let fileList: FileList = event.files;
     
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('file', file, file.name);
            let headers = new Headers();
            /** No need to include Content-Type in Angular 4 */
            // headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');

            let uriPart = `uploadfile?dataType=${fileDataType}&fileType=csv`;

            return this._http.post(this._filemannagementUrl + uriPart, formData, { headers: headers })
                .map(res => res.json())
                .do(data => console.log('Add ale result: ' + JSON.stringify(data)))
                .catch(this.handleError);
        }
    }
    approveFile(fileId: number) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      const content = JSON.stringify(fileId);
      return this._http.post(this._filemannagementUrl+fileId, content,{headers: headers})
        .map((res: Response) => res.json());
    }

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }

}
