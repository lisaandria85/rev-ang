import { Component, OnInit } from '@angular/core';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { FilemanagerService } from './filemanager.service';
import { AlertService } from '../alert/index';

import * as _ from "lodash";

@Component({
  selector: 'app-filemanager',
  templateUrl: './filemanager.component.html',
  styleUrls: ['./filemanager.component.css']
})
export class FilemanagerComponent implements OnInit {

  errorMessage: string;
  public fileDataList: Array<any> = [];
  public selectedDateType : string = '-1';
  public content = 'first';
  public startDateModel = '01/01/2017';
  public endDateModel = '12/31/2017';
  public selectedStartDate: any = { date: { year: 2017, month: 1, day: 1 } };
  public selectedEndDate: any = { date: { year: 2017, month: 12, day: 31 } };

  uploadingInProgress = false;
  public loading = false;
  fileUploadDataType: any = 'ALE';
  dataLoaded: boolean;
  public rows: Array<any> = [];
  fileName: string = "";
  public page: number = 1;
  public itemsPerPage: number = 50;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'mm/dd/yyyy',
    showClearDateBtn: false
  };

  public columns: Array<any> = [
    { title: 'Data Type', className: 'va-m', name: 'dataType' },
    { title: 'File Name', className: 'va-m', name: 'fileName' },
    { title: 'Uploaded Date', className: 'va-m', name: 'fileUploadDate' },
    { title: 'File Status', className: 'va-m', name: 'fileStatus' },
    { title: 'File Type', className: 'va-m', name: 'fileType' },
    { title: 'Has Errors', className: 'va-m', name: 'hasErrors' },
  ];

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table', 'table-striped', 'table-bordered', 'table-hover']
  };

  constructor(private _filemanagerService: FilemanagerService,  private alertService: AlertService) { }

  getFilterValues(): any {
    const filterCriteria: any = {
      selectedDateType: this.selectedDateType,
      selectedStartDate: this.startDateModel,
      selectedEndDate: this.endDateModel
    };
    return filterCriteria;
  }
  reset(): void {
    this.alertService.clear();
    this.selectedDateType = '-1';
    this.fileUploadDataType = 'ALE';
    this.selectedStartDate = { date: { year: 2017, month: 1, day: 1 } };
    this.selectedEndDate = { date: { year: 2017, month: 12, day: 31 } };
    this.startDateModel = '01/01/2017';
    this.endDateModel = '12/31/2017';
    this.dataLoaded = false;
    // this.getListOfFiles()
  }
  resetFielUpload(): void {
    this.fileUploadDataType = 'ALE';
    this.fileName = '';
  }
  getListOfFiles(): void {
    this.alertService.clear();
    this.loading = true;
    if(this.selectedDateType == '-1'|| _.isEmpty(this.selectedStartDate) || _.isEmpty(this.selectedEndDate)){
      this.alertService.error('Please select value for all required fields');
      this.loading = false;
      return;
    }
    const filterCriteria = this.getFilterValues();
    this._filemanagerService.getFilesWithFilters(filterCriteria).subscribe(data => {
      this.fileDataList = data;
      // this.onChangeTable(this.config);
      this.dataLoaded = true;
        this.loading = false;
    },
      error => {
      this.errorMessage = <any>error;
        this.alertService.error('Error in getting data');
      });
  }
  ngOnInit(): void {
    // this.getListOfFiles();
  }

  public changePage(page: any, data: Array<any> = this.fileDataList): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name] && item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;
    return filteredData;
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }
  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    let filteredData = this.changeFilter(this.fileDataList, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    //this.rows =[{"dataFileControlId":15,"dataType":"<a href='#/reporting/onboardingcustomerinformation'>test</a>","fileName":"ty2017_1095c_crosswalk_v1-201710061856044.xlsx","fileUploadDate":1507316204000,"fileStatus":"PENDING","fileType":"REPLACEMENT","hasErrors":false,"fileProcessedDate":null,"isActive":null,"isDeleted":null,"createdDate":1507341404000,"createdBy":"API","modifiedDate":1507316204000,"modifiedBy":"API"},{"dataFileControlId":19,"dataType":"ALE","fileName":"test-201710101704057.csv","fileUploadDate":1507655098000,"fileStatus":"PENDING","fileType":"csv","hasErrors":null,"fileProcessedDate":null,"isActive":"Y","isDeleted":"N","createdDate":1507655098000,"createdBy":"API","modifiedDate":1507655098000,"modifiedBy":"API"},{"dataFileControlId":20,"dataType":"ALE","fileName":"test-201710110336058.csv","fileUploadDate":1507693018000,"fileStatus":"PENDING","fileType":"csv","hasErrors":null,"fileProcessedDate":null,"isActive":"Y","isDeleted":"N","createdDate":1507693018000,"createdBy":"API","modifiedDate":1507693018000,"modifiedBy":"API"},{"dataFileControlId":21,"dataType":"ALE","fileName":"test-201710110408012.csv","fileUploadDate":1507694893000,"fileStatus":"PENDING","fileType":"csv","hasErrors":null,"fileProcessedDate":null,"isActive":"Y","isDeleted":"N","createdDate":1507694893000,"createdBy":"API","modifiedDate":1507694893000,"modifiedBy":"API"},{"dataFileControlId":14,"dataType":"ALE","fileName":"test-201710061855042.txt","fileUploadDate":1507316142000,"fileStatus":"PENDING","fileType":"REPLACEMENT","hasErrors":true,"fileProcessedDate":null,"isActive":null,"isDeleted":null,"createdDate":1507341342000,"createdBy":"API","modifiedDate":1507316142000,"modifiedBy":"API"},{"dataFileControlId":16,"dataType":"ALE","fileName":"09152017 TAC Presentation final-201710061904059.pdf","fileUploadDate":1507316700000,"fileStatus":"PENDING","fileType":"REPLACEMENT","hasErrors":true,"fileProcessedDate":null,"isActive":"Y","isDeleted":"N","createdDate":1507341900000,"createdBy":"API","modifiedDate":1507316700000,"modifiedBy":"API"},{"dataFileControlId":17,"dataType":"ALE","fileName":"ACA_demo-201710061906039.docx","fileUploadDate":1507316800000,"fileStatus":"APPROVED","fileType":"REPLACEMENT","hasErrors":true,"fileProcessedDate":null,"isActive":"Y","isDeleted":"N","createdDate":1507341999000,"createdBy":"API","modifiedDate":1507318152000,"modifiedBy":"API"}]
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;

    this.rows.forEach(item => {
     
      item.fileUploadDate = this.formatDate(item.fileUploadDate);
      if (item.hasErrors) {
        item.hasErrors = `<a href='#/reporting/fileerrors/${item.dataFileControlId}' style="color:red">${item.hasErrors}</a>`
      }
      else {
        item.fileName = `<a href='#/reporting/filesummary/${item.dataFileControlId}'>${item.fileName}</a>`
      }
    })
    this.length = sortedData.length;
  }
  onStartDateChanged(event: IMyDateModel) {
    // date selected
    this.startDateModel = event.formatted;
  }
  onEndDateChanged(event: IMyDateModel) {
    // date selected
    this.endDateModel = event.formatted;
  }

  fileChange(event: any) {
    this.fileName = event.target.files[0].name;
  }
  successMessage: any;
  submitFile(event: Event) {
    this.uploadingInProgress = true;
    this._filemanagerService.fileChange(event, this.fileUploadDataType).subscribe(data => {
        this.successMessage = 'File has been uploaded successfully';
        this.uploadingInProgress = false;
    },
      error => {
        this.alertService.error('File not uploaded');
        this.uploadingInProgress = false;
      });
    this.resetFielUpload();
  }

  resetModel() {
    this.successMessage = '';
    this.fileName = '';
  }

  formatDate(date: Date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  onChange(status: string, fileId: number) {
    this._filemanagerService.approveFile(fileId).subscribe(data => {
      console.log('Success', data.toString());
    });
  }
}
