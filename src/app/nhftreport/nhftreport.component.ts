import { Component, OnInit,ViewEncapsulation, ElementRef, ViewChild} from '@angular/core';
import { NewHireFullTimeService } from './nhftreport.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

@Component({
    moduleId: module.id,
    selector: 'nhftreport',
    templateUrl: 'nhftreport.html'
})

export class NewHireFullTimeComponent implements OnInit {
    
    topScrollWidth: number;
    // selectedYear: string;
    selectedHireMonth: string;
    selectedControlGroup: string[];
    selectedYear: string[];
    selectedMonth: string[];
    eligibleFullTimeWorkers: string = '0';
    errorMessage: string;
    downloadPdfInProgress:boolean=false;
    downloadExcelInProgress:boolean=false;

    Years: Array<string>;
    Months: Array<string>;
    ControlGroups: Array<string>;
    workerDetails: Array<any> = [];

    dataLoaded: boolean;
   
    public loading = false;
    public rows: Array<any> = [];
    public page: number = 1;
    public itemsPerPage: number = 50;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;

    optionsModel: number[];
    myOptions: IMultiSelectOption[];
    myYearOptions: IMultiSelectOption[];
    myMonthOptions: IMultiSelectOption[];
    mySettings: IMultiSelectSettings = {
        enableSearch: false,
        showCheckAll: true,
        showUncheckAll: true,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default btn-block',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false
    };

    myYearSettings: IMultiSelectSettings = {
        enableSearch: false,
        showCheckAll: true,
        showUncheckAll: true,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default btn-block',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false
    };

    myMonthSettings: IMultiSelectSettings = {
        enableSearch: false,
        showCheckAll: true,
        showUncheckAll: true,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default btn-block',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false
    };

    // Text configuration
    myTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all'
    };

      myYearTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all'
    };

      myMonthTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all'
    };
    @ViewChild('topScroll') topScroll: ElementRef;
    @ViewChild('bottomScroll') bottomScroll: ElementRef;

    public columns: Array<any> = [
        { title: 'Controlled Group', className: 'va-m', name: 'controlGroup' },
        { title: 'Latest Production Company', className: 'va-m', name: 'mostRecentProductionCompany' },
        { title: 'Most Recent Show', className: 'va-m', name: 'mostRecentProject' },
        { title: 'SSN Number', className: 'hidden-xs va-m', name: 'ssnNumber' },
        { title: 'First Name', className: 'hidden-xs va-m', name: 'firstName' },
        { title: 'Last Name', className: 'va-m', name: 'lastName' },
        { title: 'Last Worked Date', className: 'va-m', name: 'lastWorkedDate' },
        { title: 'Hire Date', className: 'va-m', name: 'hireDate' },
        { title: 'Union Type', className: 'va-m', name: 'unionType' },
        { title: 'Payroll Source', className: 'va-m', name: 'payrollSource' },
        { title: 'Employment Status', className: 'va-m', name: 'employmentStatus' },
        { title: 'Average Hours', className: 'va-m', name: 'avgHours' },
        { title: 'Total Hours', className: 'va-m', name: 'totalHours' },
      


    ];
    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table', 'table-striped', 'table-bordered', 'table-hover']
    };

    constructor(private _newHireFullTimeService: NewHireFullTimeService) { }

    ngOnInit(): void {
       // this.myOptions = [{ id: '-1', name: 'All' }];
       this.myOptions = [];
       this.myYearOptions = [];
       this.myMonthOptions = [];
        this._newHireFullTimeService.getReportData().subscribe(data => {

            this.Years = data.WorkYear;
            this.Months = data.WorkMonth;
            this.ControlGroups = data.ControlGroup;
            data.ControlGroup.forEach(element => {
                this.myOptions.push({ id: element, name: element })
            });

            data.WorkYear.forEach(element => {
                this.myYearOptions.push({ id: element, name: element })
            });
            data.WorkMonth.forEach(element => {
                this.myMonthOptions.push({ id: element, name: element })
            });
        },
            error => this.errorMessage = <any>error);

        // this.selectedYear = '-1';
        // this.selectedHireMonth = '-1';
       // this.selectedControlGroup = ['-1'];
        this.eligibleFullTimeWorkers = '0';

        this.onChangeTable(this.config);
        this.reset();
        

        //this.dataLoaded = false;
       
        

    }



    eligibleFullTimeReportData(): void {
        this.loading=true;
        
        let filterCriteria = this.getFilterValues();
        
        //this.loading=true;
       
        filterCriteria.acaEligibleCount = this.eligibleFullTimeWorkers;
        this.dataLoaded = true;
        this._newHireFullTimeService.getEligibleFullTimeReportData(filterCriteria).subscribe(workdetails => {
            this.loading = false;
            this.workerDetails = workdetails;
            this.onChangeTable(this.config);
            
            this.topScrollWidth = this.bottomScroll.nativeElement.scrollWidth;
           // this.dataLoaded=true;
            //this.loading=false;
            
            
            
        },
            error => this.errorMessage = <any>error);

    }

    reset(): void {
        // this.selectedYear = '-1';
        //this.selectedHireMonth = '-1';
        this.selectedControlGroup = [];
        this.selectedYear = [];
        this.selectedMonth = [];
        this.dataLoaded = false;
        this.eligibleFullTimeWorkers = '0'
    }

    getFilterValues(): any {
        // let year = this.selectedYear;
        // if (year === '-1') {
        //     year = '\'\'';
        // }
        // let month = this.selectedHireMonth;
        // if (month === '-1') {
        //     month = '\'\'';
        // }

        //Control Group Multi Select
        let cg = this.selectedControlGroup;
        if ( cg === undefined) {
                cg = [];
                cg[0] = '\'\'';
                this.selectedControlGroup = [];
            }
        if (cg !== undefined && cg.length <= 0) {
                cg[0] = '\'\'';
                this.selectedControlGroup = [];
            }

        //Year multi select
        let cgYear = this.selectedYear;
        if ( cgYear === undefined) {
                cgYear = [];
                cgYear[0] = '\'\'';
                this.selectedYear = [];
            }
        if (cgYear !== undefined && cgYear.length <= 0) {
                cgYear[0] = '\'\'';
                this.selectedYear = [];
            }

        //Month multi select
        let cgMonth = this.selectedMonth;
        if ( cgMonth === undefined) {
                cgMonth = [];
                cgMonth[0] = '\'\'';
                this.selectedMonth = [];
            }
        if (cgMonth !== undefined && cgMonth.length <= 0) {
                cgMonth[0] = '\'\'';
                this.selectedMonth = [];
            }


        const filterCriteria: any = {
            selectedYear: cgYear, selectedHireMonth: cgMonth, selectedControlGroup: cg.join(':')
        };

        return filterCriteria;
    }

    Search(): void {
     
        //this.dataLoaded = false;
        let filterCriteria = this.getFilterValues();
        this.eligibleFullTimeWorkers = '0';
        this._newHireFullTimeService.getEligibleFullTimeWorkers(filterCriteria)
            .subscribe(counts => {
                if (counts === undefined || counts == null) {
                    return;
                }
                counts.forEach((element: any) => {
                    this.eligibleFullTimeWorkers = element.acaEligibleCount;
                });

            },
            (error: any) => this.errorMessage = <any>error);


    }


    downloadPdf(): void {
        this.downloadPdfInProgress=true;
        let filterCriteria = this.getFilterValues();
        this._newHireFullTimeService.downloadPDFReport(filterCriteria).subscribe(data=>{this.downloadPdfInProgress=false
        },err=>(this.downloadPdfInProgress=false))
    }

    downloadExcel(): void {
        this.downloadExcelInProgress=true;
        let filterCriteria = this.getFilterValues();
        this._newHireFullTimeService.downloadExcelReport(filterCriteria).subscribe(data=>{this.downloadExcelInProgress=false
        },err=>(this.downloadExcelInProgress=false))
    }
    public onCellClick(data: any): any {
        console.log(data);
    }

    public changePage(page: any, data: Array<any> = this.workerDetails): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }


    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name].toLowerCase().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }
    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.workerDetails, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }


    arescrolling = 0;
    scroller(from: any, to: any) {
        if (this.arescrolling) return; // avoid potential recursion/inefficiency
        this.arescrolling = 1;
        // set the other div's scroll position equal to ours
        // this.el;
        // debugger;
        from.scrollLeft = to.scrollLeft;
        // document.getElementById(to).scrollLeft =
        //     document.getElementById(from).scrollLeft;
        this.arescrolling = 0;
    }
}
