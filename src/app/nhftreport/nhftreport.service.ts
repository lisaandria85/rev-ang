
import { Injectable } from '@angular/core';
import { Http, Response ,Headers,ResponseContentType} from '@angular/http';
import { InhftWorkDetail } from './nhftworkdetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class NewHireFullTimeService {
    private _nhftreportUrl = CONFIGURATION.baseServiceUrl + 'newhiresfulltimereportservice/';
    constructor(private _http: Http) { }


    getReportData(): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(this._nhftreportUrl + 'getNewHiresFullTimeReportReferenceData',{headers:headers})
            .map((response: Response) => response.json().EligibilityNewHiresFullTimeReferenceData)
            //.do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getEligibleFullTimeWorkers(filterCriteria: any): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName: string = "getNewHiresFullTimeCountByWeek?WorkYear=" + filterCriteria.selectedYear
            + "&WorkMonth=" + filterCriteria.selectedHireMonth
            + "&ControlGroup=" + filterCriteria.selectedControlGroup;
        return this._http.get(this._nhftreportUrl + fileName,{headers:headers})
            .map((response: Response) => response.json().summaryCountForNewHireFullTimeVO)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getEligibleFullTimeReportData(filterCriteria: any): Observable<InhftWorkDetail[]> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');

        let fileName = "getNewHiresFullTimeReportData?WorkYear=" + filterCriteria.selectedYear
            + "&WorkMonth=" + filterCriteria.selectedHireMonth
            + "&ControlGroup=" + filterCriteria.selectedControlGroup;
        return this._http.get(this._nhftreportUrl + fileName,{headers:headers})
            .map((response: Response) => <InhftWorkDetail[]>response.json().reportByACAEligibleCount)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    downloadPDFReport(filterCriteria: any): any {
        let fileName = "processNewHiresFullTimeReportPDFUpload?WorkYear=" + filterCriteria.selectedYear
            + "&WorkMonth=" + filterCriteria.selectedHireMonth
            + "&ControlGroup=" + filterCriteria.selectedControlGroup;

        //window.open(this._nhftreportUrl + fileName, '_bank');
        const url = this._nhftreportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        // headers.append('Authorization', 'JWT ' + localStorage.getItem('id_token'));
       return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }
      downloadExcelReport(filterCriteria: any): any {
        let fileName = "processNewHireFullTimeExcelUpload?WorkYear=" + filterCriteria.selectedYear
            + "&WorkMonth=" + filterCriteria.selectedHireMonth
            + "&ControlGroup=" + filterCriteria.selectedControlGroup;

        //window.open(this._nhftreportUrl + fileName, '_bank');
        const url = this._nhftreportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }


}
