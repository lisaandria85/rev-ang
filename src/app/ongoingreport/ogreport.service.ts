import { Injectable } from '@angular/core';
import { Http, Response,Headers,ResponseContentType } from '@angular/http';
import { IWorkDetails } from './workdetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';
@Injectable()
export class OnGoingReportService {
    private _onGoingReportUrl = CONFIGURATION.baseServiceUrl + 'ongoingreportservice/';

    constructor(private _http: Http) {

    }

    getEndDatesOfCG(controlGroup: any): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let url : string = 'getMeasurementDatesByControlGroup?ControlGroup=' + controlGroup ;
        return this._http.get(this._onGoingReportUrl + url,{headers:headers} )
            .map((response: Response) => response.json().measurementDateByControlGroupVO)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getReportData(): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(this._onGoingReportUrl + 'getonGoingreportreferencedata',{headers:headers})
            .map((response: Response) => response.json().ongoingReportVO)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }


    getOnGoingReportDataCount(filterCriteria: any): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName: string = 'getOnGoingReportCountByWeek?MeasurementStartDate=' + filterCriteria.selectedMeasuredStartDate
            + '&MeasurementEndDate=' + filterCriteria.selectedMeasuredEndDate
            + '&AvgWeeklyHours=' + filterCriteria.avgWeeklyThreshold
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&TypeOfHours=' + filterCriteria.selectedTypeOfHours;

        return this._http.get(this._onGoingReportUrl + fileName,{headers:headers})
            .map((response: Response) => response.json().ongoingCountVO)
            //.do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getOnGoingReportData(filterCriteria: any): Observable<IWorkDetails[]> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName = 'getOnGoingReportReportData?MeasurementStartDate=' + filterCriteria.selectedMeasuredStartDate
            + '&MeasurementEndDate=' + filterCriteria.selectedMeasuredEndDate
            + '&AvgWeeklyHours=' + filterCriteria.avgWeeklyThreshold
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&TypeOfHours=' + filterCriteria.selectedTypeOfHours;
           // + '&ReportOfWeek=' + filterCriteria.reportCount;

        return this._http.get(this._onGoingReportUrl + fileName,{headers:headers})
            .map((response: Response) => <IWorkDetails[]>response.json().onGoingReportDataVO.onGoingReportsByWeekCount)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    downloadPDFReport(filterCriteria: any): any {
        let fileName = 'processOnGoingReportPDFUpload?MeasurementStartDate=' + filterCriteria.selectedMeasuredStartDate
            + '&MeasurementEndDate=' + filterCriteria.selectedMeasuredEndDate
            + '&AvgWeeklyHours=' + filterCriteria.avgWeeklyThreshold
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&TypeOfHours=' + filterCriteria.selectedTypeOfHours;
          //  + '&ReportOfWeek=' + filterCriteria.reportCount;

        //window.open(this._onGoingReportUrl + fileName, '_bank');
        const url = this._onGoingReportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        // headers.append('Authorization', 'JWT ' + localStorage.getItem('id_token'));
       return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }
    downloadExcelReport(filterCriteria: any): any {
        let fileName = 'processOnGoingReportExcelUpload?MeasurementStartDate=' + filterCriteria.selectedMeasuredStartDate
            + '&MeasurementEndDate=' + filterCriteria.selectedMeasuredEndDate
            + '&AvgWeeklyHours=' + filterCriteria.avgWeeklyThreshold
            + '&ControlGroup=' + filterCriteria.selectedControlGroup
            + '&TypeOfHours=' + filterCriteria.selectedTypeOfHours;
          //  + '&ReportOfWeek=' + filterCriteria.reportCount;

        //window.open(this._onGoingReportUrl + fileName, '_bank');
        const url = this._onGoingReportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    }
    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
