import { Component, OnInit , ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OnGoingReportService } from './ogreport.service';

@Component({
    moduleId: module.id,
    templateUrl: './ogreport.html'

})
export class OnGoingReportComponent implements OnInit {
    topScrollWidth: number;
    dataLoaded: boolean;
    public loading=false;

    ogReportForm: FormGroup;
    private controlGroupControl: FormControl;
    private measurementEndDateControl: FormControl;
    private measurementStartDateControl: FormControl;
    private avgWeeklyThresholdControl: FormControl;
    private typeOfHoursControl: FormControl;
   
    
    downloadPdfInProgress:boolean=false;
    downloadExcelInProgress:boolean=false;

    

    
    measurementStartDates: Array<string>;
    measurementEndDates: Array<string>;
    controlGroups: Array<string>;
    typeOfHours: Array<string>;

    errorMessage: string;
    countWeeks: string;
    countMonths: string;
   

    // TODO - use of below?
    public count13Weeks: any;
    public count26Weeks: any;
    public count47Weeks: any;
    // end
    avgWeeklyThreshold:Array<string>;
    workDetails: Array<any> = [];

    @ViewChild('topScroll') topScroll: ElementRef;
    @ViewChild('bottomScroll') bottomScroll: ElementRef;

    public rows: Array<any> = [];
    public columns: Array<any> = [
        { title: 'Controlled Group', className: 'va-m', name: 'controlGroup' },
        { title: 'Latest Production Company', className: 'va-m', name: 'mostRecentProductionCompany' },
        { title: 'Most Recent Show', className: 'va-m', name: 'mostRecentProject' },
        { title: 'SSN Number', className: 'hidden-xs va-m', name: 'ssnNumber' },
        { title: 'First Name', className: 'hidden-xs va-m', name: 'firstName' },
        { title: 'Last Name', className: 'va-m', name: 'lastName' },
        { title: 'Last Worked Date', className: 'va-m', name: 'lastWorkedDate' },
        { title: 'Hire Date', className: 'va-m', name: 'hireDate' },
        { title: 'Union/Non-Union', className: 'va-m', name: 'unionType' },
        { title: 'Weeks Since Last Worked', className: 'va-m', name: 'weeksSinceLastWorked' },
        { title: 'Average Hours-SMP', className: 'va-m', name: 'avgHours' },
        { title: 'Total Hours', className: 'va-m', name: 'totalHours' },
        { title: 'Employee Type', className: 'va-m', name: 'employeeType' }
    ];
    public page: number = 1;
    public itemsPerPage: number = 50;
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table', 'table-striped', 'table-bordered', 'table-hover']
    };

    constructor(private _ogreportsrv: OnGoingReportService) {

    }

    ngOnInit(): void {
        this.countWeeks = '0';
        this.controlGroupControl = new FormControl('', Validators.required);
        this.typeOfHoursControl = new FormControl('', Validators.required);
        this.measurementStartDateControl = new FormControl('', Validators.required);
        this.measurementEndDateControl = new FormControl('', Validators.required);
        this.avgWeeklyThreshold=['>=30','<30'];
       
        this.avgWeeklyThresholdControl = new FormControl('', Validators.required);
        
        //  this.selectedweekCount = 0;
        this.ogReportForm = new FormGroup(
            {
                controlGroup: this.controlGroupControl,
                typeOfHour: this.typeOfHoursControl,

                avgWeeklyHoursThreshold: this.avgWeeklyThresholdControl,
                measurementEndDate: this.measurementEndDateControl,
                measurementStartDate: this.measurementStartDateControl
            }
        );

        this._ogreportsrv.getReportData().subscribe(data => {

            this.controlGroups = data.ControlGroup;
            this.typeOfHours = data.typeOfHours;
        },
            error => this.errorMessage = <any>error);

        this.countWeeks = '0';
        this.countMonths = '0';
        this.onChangeTable(this.config);
        this.reset();

        //this.dataLoaded = false;
    }

    endDatesOfCG($event) {
        this._ogreportsrv.getEndDatesOfCG(this.controlGroupControl.value).subscribe(data => {

            this.measurementStartDates = data.measurementStartDatesByControlGroupList;
            this.measurementEndDates = data.measurementEndDatesByControlGroupList;
        }, error => this.errorMessage = <any>error);
    }

    reset(): void {
        
        this.typeOfHoursControl.setValue('');
        this.controlGroupControl.setValue('');
        this.measurementEndDateControl.setValue('');
        this.measurementStartDateControl.setValue('');
        this.avgWeeklyThresholdControl.setValue('');
        this.dataLoaded = false;
        this.countWeeks = '0';
        this.countMonths = '0';
        this.dataLoaded = false;

    }

    getFilterValues(): any {
        let measurementStartDate = this.measurementStartDateControl.value;
        if (measurementStartDate === undefined || measurementStartDate === '') {
            measurementStartDate = '\'\'';
        }
        let measurementEndDate = this.measurementEndDateControl.value;
        if (measurementEndDate === undefined || measurementEndDate === '') {
            measurementEndDate = '\'\'';
        }
        let cg = this.controlGroupControl.value;
        if (cg === undefined || cg === "All" || cg === '') {
            cg = '\'\'';
        }
        let emptype = this.typeOfHoursControl.value;
        if (emptype === undefined || emptype === '') {
            emptype = '\'\'';
        }
        let cat = this.avgWeeklyThresholdControl.value;
        if (cat === undefined || cat === '') {
            cat = '\'\'';
        }

        let filterCriteria: any = {
        
            selectedMeasuredStartDate: measurementStartDate,
            selectedMeasuredEndDate: measurementEndDate,
            selectedControlGroup: cg,
            selectedTypeOfHours: emptype,
            avgWeeklyThreshold: cat,
            reportCount: this.countWeeks
        };

        return filterCriteria;
    }

    Search(): void {
        //this.dataLoaded = false;
        let filterCriteria = this.getFilterValues();
        console.log(filterCriteria);
        this.countWeeks = '0';
        this.countMonths = '0';
        this._ogreportsrv.getOnGoingReportDataCount(filterCriteria).subscribe(data => {
            this.countMonths = data.onGoingMonthDiffCount;
            
            if (data.onGoingReportCount == undefined)
                this.countWeeks = '0';
            else
                this.countWeeks = data.onGoingReportCount;

        },
            (error: any) => this.errorMessage = <any>error);
    }

    getWeekData(item: number = 0): void {
        this.loading = true;
        
        let filterCriteria = this.getFilterValues();
        this.dataLoaded=true;
        this._ogreportsrv.getOnGoingReportData(filterCriteria).subscribe(data => {
            this.loading=false;
            this.workDetails = data;
            this.onChangeTable(this.config);
            this.topScrollWidth = this.bottomScroll.nativeElement.scrollWidth;
            // this.dataLoaded = true;
            // this.loading=false;
        },
            error => this.errorMessage = <any>error);

    }

    downloadPdf(): void {
        this.downloadPdfInProgress=true;
        let filterCriteria = this.getFilterValues();
        this._ogreportsrv.downloadPDFReport(filterCriteria).subscribe(data=>{this.downloadPdfInProgress=false;
                },err=>(this.downloadPdfInProgress=false))
    }

    downloadExcel(): void {
        this.downloadExcelInProgress=true;
        let filterCriteria = this.getFilterValues();
        this._ogreportsrv.downloadExcelReport(filterCriteria).subscribe(data=>{this.downloadExcelInProgress=false;
                },err=>(this.downloadExcelInProgress=false))
    }
    // Validations

    validateControlGroups(): boolean {
        return this.controlGroupControl.valid || this.controlGroupControl.untouched;
    }

    validateMeasurementEndDate(): boolean {
        return this.measurementEndDateControl.valid || this.measurementEndDateControl.untouched;
    }

    validateAvgThreashold(): boolean {
        return this.avgWeeklyThresholdControl.valid || this.avgWeeklyThresholdControl.untouched;
    }

    validateTypeOfHour(): boolean {
        return this.typeOfHoursControl.valid || this.typeOfHoursControl.untouched;
    }

    public onCellClick(data: any): any {
        console.log(data);
    }

    public changePage(page: any, data: Array<any> = this.workDetails): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }


    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        let tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name].toLowerCase().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.changeFilter(this.workDetails, this.config);
        let sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }
    arescrolling = 0;
    scroller(from: any, to: any) {
        if (this.arescrolling) return; // avoid potential recursion/inefficiency
        this.arescrolling = 1;
        from.scrollLeft = to.scrollLeft;
        this.arescrolling = 0;
    }

}
