import { Component, OnInit } from '@angular/core';
import { irsfileService } from './irsfile-service';
import { SharedService } from '../app.shared.service';
import { Router } from '@angular/router';

import { AlertService } from '../alert/index';
import { CONFIGURATION } from '../app.config';

import * as _ from "lodash";
import {logging} from "selenium-webdriver";

@Component({
  selector: 'app-irs-file-management',
  templateUrl: './irs-file-management.component.html',
  styleUrls: ['./irs-file-management.component.css'],
  providers: [irsfileService],
})
export class IrsFileManagementComponent implements OnInit {
  public taxYearList: any[];
  public einList: any[];
  public fileIRSList: any[];
  dataLoaded: boolean;
  public loading = false;
  downloadLink: string = CONFIGURATION.baseIrsstatusURL+'1094/xml/download?id=';

  public selectedYear: number = -1;
  public selectedEIN: number = -1;

  constructor(private irsFileServ: irsfileService,
              private sharedService: SharedService,
              private router: Router, private alertService: AlertService) {
  }

  ngOnInit() {
    this.irsFileServ.getIRSReferenceData().subscribe(data => {
      this.einList = data.einList;
      this.taxYearList = data.taxYearList;
    });
  }
 reset(): void {
    this.dataLoaded = false;
    this.selectedYear = -1;
    this.selectedEIN = -1;
    this.alertService.clear();
  }


  getAllIRSBySearchCriteria() {
    this.alertService.clear();
    this.loading = true;
    if(this.selectedYear == -1 || this.selectedEIN == -1){
      this.alertService.error('Please select year and EIN');
      this.loading = false;
      return;
    }
    this.irsFileServ.getAllIRSBySearchCriteria(this.selectedYear, this.selectedEIN).subscribe(data => {
      this.fileIRSList = data;
      _.forEach(this.fileIRSList, (irsFile) => {
        irsFile.createdDate = this.formatDate(irsFile.createdDate);
        irsFile.modifiedDate = this.formatDate(irsFile.modifiedDate);
      });
      this.dataLoaded = true;
      this.loading = false;
    },
      (error) => {
      console.log('Error', error);
      this.loading = false;

    });
  }

  shareObj(data: any) {
    this.sharedService.setObj(data);
    this.router.navigate(['/reporting/irs-file-management']);
  }

  formatDate(date: Date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  onChange(selectStatus: string, data: any) {
    data.irsSubmissionUserAction = selectStatus;
    console.log('hello', data);
    this.irsFileServ.updateIRSStatus(data).subscribe(data => {
      console.log('Success', data.toString());
    });
  }
}
