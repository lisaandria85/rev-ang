import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IrsFileManagementComponent } from './irs-file-management.component';

describe('IrsFileManagementComponent', () => {
  let component: IrsFileManagementComponent;
  let fixture: ComponentFixture<IrsFileManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IrsFileManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IrsFileManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
