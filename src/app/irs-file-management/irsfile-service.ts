import { Injectable } from '@angular/core';
import { Http, Response, Headers, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class irsfileService {
    private _irsFilemanagement = CONFIGURATION.baseIrsstatusURL;

    constructor(private http: Http) { }

    getIRSReferenceData() {
        return this.http.get(this._irsFilemanagement+'1094/status/' + 'referencedata')
            .map((res: Response) => res.json());
    }

    getAllIRSBySearchCriteria(taxYear: number, ein:number) {
        return this.http.get(this._irsFilemanagement + '1094/status/?taxYear=' + taxYear + '&ein=' + ein)
            .map((res: Response) => res.json());
    }
    updateIRSStatus(irsFileObj: any) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      const content = JSON.stringify(irsFileObj);
      return this.http.put(this._irsFilemanagement+'1094/status/', content,{headers: headers})
        .map((res: Response) => res.json());
    }
}
