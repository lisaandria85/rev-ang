import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

import { tokenNotExpired } from "angular2-jwt";

import { LoginModel } from "./LoginModel";
import { Http, Response, Headers } from "@angular/http";
import { CONFIGURATION } from "../app.config";
//import { retry } from "rxjs/operators/retry";

@Injectable()
export class LoginService {
  private _LoginUrl = CONFIGURATION.baseloginhicURL;

  constructor(private _http: Http) {}

  authenticate(newObj: LoginModel) {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    const content = JSON.stringify(newObj);
    return this._http
      .get(
        this._LoginUrl + `user=${newObj.userName}&password=${newObj.password}`,
        { headers: headers }
      )
      .map((response: Response) => response.json())
      .do(data => {
        if (data.status) {
          // console.log('Error');
          return data;
        } else {
          throw new Error('This request has failed' + data.status);
        }

        // console.log('Login API result ' + JSON.stringify(data))
      })
      .catch(this.handleError);
  }
  private handleError(error: Response) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    // console.error("rahul " + error);
    return Observable.throw(error.json().error || "Server error");
  }
  loggedIn() {
    return tokenNotExpired();
  }
}
