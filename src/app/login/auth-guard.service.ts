import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { CanActivate } from "@angular/router";
import { LoginService } from "./login.service";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: LoginService, private router: Router) {}

  canActivate() {
    localStorage.getItem("token");
    // TODO - uncomment this after JWT intro
    // if(this.auth.loggedIn()) {
    if (localStorage.getItem("token")) {
      return true;
    } else {
      this.router.navigateByUrl("/unauthorized");
      return false;
    }
  }
}
