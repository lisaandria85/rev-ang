import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as _ from "lodash";

import { LoginService } from './login.service';
import { LoginModel } from './LoginModel';
import { AlertService } from '../alert/index';

@Component({
  moduleId: module.id,
  templateUrl: './login.html',
})
export class LoginComponent implements OnInit {
  route = 'reporting';
  dashboard = 'dashboard';
  model: any = {};
  error: any= '';
  loginModel: LoginModel;
  message: string;
  errorMessage: string;
  public loading = false;
  constructor(private _router: Router, private _service: LoginService, private alertService: AlertService) { }
  ngOnInit(): void {
    localStorage.removeItem('token');
  }
  populateModel(model: any) {
    this.loginModel = <LoginModel>{};
    this.loginModel.userName = model.username;
    this.loginModel.password = model.password;
  }
  login(): void {
    this.alertService.clear();
    this.loading = true;
    this.populateModel(this.model);
    this._service.authenticate(this.loginModel).subscribe(data => {
      this.alertService.clear();
      this.loading = false;
      if (!_.isEmpty(data)) {
        localStorage.setItem('token', data.status),
          this._router.navigate(['/reporting/dashboard']);
        // console.log('login/successful');
      }
    }, error => {
      this.loading = false;
      this.errorMessage = 'The username and password supplied is incorrect';
      this.alertService.error(this.errorMessage);
    });

    /*if (this.model.username.toLowerCase() === 'admin' && this.model.password.toLowerCase() === 'admin') {
      this._router.navigate(['/reporting/dashboard']);
      console.log('login/submitted');
    } else {
      this.error = 'Username or password is incorrect';
      console.log(this.error);
    }*/
  }
}
