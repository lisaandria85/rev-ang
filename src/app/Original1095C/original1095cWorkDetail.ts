export interface IOriginal1095cWorkDetail {
    employeeName: string;
    employeeFirstName: string;
    employeeMiddleName: string;
    employeeLastName: string;
    employeeSSN: string;
    employeeAddressLine1: string;
    employeeAddressLine2: string;
    employeeCity: string;
    employeeState: string;
    employeeCountry: String;
    employeeZip: string;
    employerName: string;
    employerFEIN: string;
    employerAddressLine1: string;
    employerAddressLine2: string;
    employerAddressLine3: string;
    employerContactPhone: string;
    employerCity: string;
    employerState: string;
    employerCountry: string;
    employerZip: string;
    selfInsuranceCoverage: string;
    controlGroup: string;
    taxYear: string;
    line14AllMonths: string;
    line14Jan: string;
    line14Feb: string;
    line14Mar: string;
    line14Apr: string;
    line14May: string;
    line14June: string;
    line14July: string;
    line14Aug: string;
    line14Sept: string;
    line14Oct: string;
    line14Nov: string;
    line14Dec: string;

    line15AllMonths: string;
    line15Jan: string;
    line15Feb: string;
    line15Mar: string;
    line15Apr: string;
    line15May: string;
    line15June: string;
    line15July: string;
    line15Aug: string;
    line15Sept: string;
    line15Oct: string;
    line15Nov: string;
    line15Dec: string;

    line16AllMonths: string;
    line16Jan: string;
    line16Feb: string;
    line16Mar: string;
    line16Apr: string;
    line16May: string;
    line16June: string;
    line16July: string;
    line16Aug: string;
    line16Sept: string;
    line16Oct: string;
    line16Nov: string;
    line16Dec: string;

    line17FirstName: string;
    line17LastName: string;
    line17SSN: string;
    line17DateOfBirth: string;
    line17AllMonths: string;
    line17Jan: string;
    line17Feb: string;
    line17Mar: string;
    line17Apr: string;
    line17May: string;
    line17June: string;
    line17July: string;
    line17Aug: string;
    line17Sept: string;
    line17Oct: string;
    line17Nov: string;
    line17Dec: string;

    line18FirstName: string;
    line18LastName: string;
    line18SSN: string;
    line18DateOfBirth: string;
    line18AllMonths: string;
    line18Jan: string;
    line18Feb: string;
    line18Mar: string;
    line18Apr: string;
    line18May: string;
    line18June: string;
    line18July: string;
    line18Aug: string;
    line18Sept: string;
    line18Oct: string;
    line18Nov: string;
    line18Dec: string;


    line19FirstName: string;
    line19LastName: string;
    line19SSN: string;
    line19DateOfBirth: string;
    line19AllMonths: string;
    line19Jan: string;
    line19Feb: string;
    line19Mar: string;
    line19Apr: string;
    line19May: string;
    line19June: string;
    line19July: string;
    line19Aug: string;
    line19Sept: string;
    line19Oct: string;
    line19Nov: string;
    line19Dec: string;

    line20FirstName: string;
    line20LastName: string;
    line20SSN: string;
    line20DateOfBirth: string;
    line20AllMonths: string;
    line20Jan: string;
    line20Feb: string;
    line20Mar: string;
    line20Apr: string;
    line20May: string;
    line20June: string;
    line20July: string;
    line20Aug: string;
    line20Sept: string;
    line20Oct: string;
    line20Nov: string;
    line20Dec: string;

    line21FirstName: string;
    line21LastName: string;
    line21SSN: string;
    line21DateOfBirth: string;
    line21AllMonths: string;
    line21Jan: string;
    line21Feb: string;
    line21Mar: string;
    line21Apr: string;
    line21May: string;
    line21June: string;
    line21July: string;
    line21Aug: string;
    line21Sept: string;
    line21Oct: string;
    line21Nov: string;
    line21Dec: string;

    line22FirstName: string;
    line22LastName: string;
    line22SSN: string;
    line22DateOfBirth: string;
    line22AllMonths: string;
    line22Jan: string;
    line22Feb: string;
    line22Mar: string;
    line22Apr: string;
    line22May: string;
    line22June: string;
    line22July: string;
    line22Aug: string;
    line22Sept: string;
    line22Oct: string;
    line22Nov: string;
    line22Dec: string;

}
