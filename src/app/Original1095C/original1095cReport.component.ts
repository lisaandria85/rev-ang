import { Component, OnInit } from '@angular/core';
import { Original1095cService } from './original1095cReport.service';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

@Component({
    moduleId: module.id,
    templateUrl: 'original1095cReport.html'

})
export class Original1095cReportComponent implements OnInit {

    dataLoaded: boolean;
    selectedYear: string[];
    selectedControlGroup: string[];
    Years: Array<string>;
    ControlGroups: Array<string>;

    public rows: Array<any> = [];
    public page = 1;
    public itemsPerPage = 10;
    public maxSize = 5;
    public numPages = 1;
    public length = 0;

    optionsModel: number[];
    myOptionsYear: IMultiSelectOption[];
    myOptionsCG: IMultiSelectOption[];

    mySettings: IMultiSelectSettings = {
        enableSearch: false,
        showCheckAll: true,
        showUncheckAll: true,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default btn-block',
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false
    };

    // Text configuration
    myTexts: IMultiSelectTexts = {
        checkAll: 'Select all',
        uncheckAll: 'Unselect all'
    };

    public columns: Array<any> = [
        { title: 'Employee Name', className: 'va-m', name: 'employeeName' },
        { title: 'Employee First Name', className: 'va-m', name: 'employeeFirstName' },
        { title: 'Employee Middle Name', className: 'va-m', name: 'employeeMiddleName' },
        { title: 'Employee Last Name', className: 'va-m', name: 'employeeLastName' },
        { title: 'Employee Address Line1', className: 'va-m', name: 'employeeAddressLine1' },
        { title: 'Employee Address Line2', className: 'va-m', name: 'employeeAddressLine2' },
        { title: 'City', className: 'va-m', name: 'employerCity' },
        { title: 'State', className: 'va-m', name: 'employerState' },
        { title: 'Country', className: 'va-m', name: 'employerCountry' },
        { title: 'Zip', className: 'va-m', name: 'employeeZip' },
        { title: 'Employer Name', className: 'va-m', name: 'employerName' },
        { title: 'Employer FEIN', className: 'va-m', name: 'employerFEIN' },
        { title: 'Employer Address Line1', className: 'va-m', name: 'employerAddressLine1' },
        { title: 'Employer Address Line2', className: 'va-m', name: 'employerAddressLine2' },
        { title: 'Employer Address Line3', className: 'va-m', name: 'employerAddressLine3' },
        { title: 'Employer Contact Phone', className: 'va-m', name: 'employerContactPhone' },
        { title: 'Employer City', className: 'va-m', name: 'employerCity' },
        { title: 'Employer State', className: 'va-m', name: 'employerState' },
        { title: 'Employer Country', className: 'va-m', name: 'employerCountry' },
        { title: 'Employer Zip', className: 'va-m', name: 'employerZip' },
        { title: 'Self Insurance Coverage', className: 'va-m', name: 'selfInsuranceCoverage' },
        { title: 'Control Group', className: 'va-m', name: 'controlGroup' },
        { title: 'Tax Year', className: 'va-m', name: 'taxYear' },
        { title: 'Line 14 All Months', className: 'va-m', name: 'line14AllMonths' },
        { title: 'Line 14 Jan', className: 'va-m', name: 'line14Jan' },
        { title: 'Line 14 Feb', className: 'va-m', name: 'line14Feb' },
        { title: 'Line 14 Mar', className: 'va-m', name: 'line14Mar' },
        { title: 'Line 14 Apr', className: 'va-m', name: 'line14Apr' },
        { title: 'Line 14 May', className: 'va-m', name: 'line14May' },
        { title: 'Line 14 June', className: 'va-m', name: 'line14June' },
        { title: 'Line 14 July', className: 'va-m', name: 'line14July' },
        { title: 'Line 14 Aug', className: 'va-m', name: 'line14Aug' },
        { title: 'Line 14 Sept', className: 'va-m', name: 'line14Sept' },
        { title: 'Line 14 Oct', className: 'va-m', name: 'line14Oct' },
        { title: 'Line 14 Nov', className: 'va-m', name: 'line14Nov' },
        { title: 'Line 14 Dec', className: 'va-m', name: 'line14Dec' },
        { title: 'Line 15 All Months', className: 'va-m', name: 'line15AllMonths' },
        { title: 'Line 15 Jan', className: 'va-m', name: 'line15Jan' },
        { title: 'Line 15 Feb', className: 'va-m', name: 'line15Feb' },
        { title: 'Line 15 Mar', className: 'va-m', name: 'line15Mar' },
        { title: 'Line 15 Apr', className: 'va-m', name: 'line15Apr' },
        { title: 'Line 15 May', className: 'va-m', name: 'line15May' },
        { title: 'Line 15 June', className: 'va-m', name: 'line15June' },
        { title: 'Line 15 July', className: 'va-m', name: 'line15July' },
        { title: 'Line 15 Aug', className: 'va-m', name: 'line15Aug' },
        { title: 'Line 15 Sept', className: 'va-m', name: 'line15Sept' },
        { title: 'Line 15 Oct', className: 'va-m', name: 'line15Oct' },
        { title: 'Line 15 Nov', className: 'va-m', name: 'line15Nov' },
        { title: 'Line 15 Dec', className: 'va-m', name: 'line15Dec' },
        { title: 'Line 16 All Months', className: 'va-m', name: 'line16AllMonths' },
        { title: 'Line 16 Jan', className: 'va-m', name: 'line16Jan' },
        { title: 'Line 16 Feb', className: 'va-m', name: 'line16Feb' },
        { title: 'Line 16 Mar', className: 'va-m', name: 'line16Mar' },
        { title: 'Line 16 Apr', className: 'va-m', name: 'line16Apr' },
        { title: 'Line 16 May', className: 'va-m', name: 'line16May' },
        { title: 'Line 16 June', className: 'va-m', name: 'line16June' },
        { title: 'Line 16 July', className: 'va-m', name: 'line16July' },
        { title: 'Line 16 Aug', className: 'va-m', name: 'line16Aug' },
        { title: 'Line 16 Sept', className: 'va-m', name: 'line16Sept' },
        { title: 'Line 16 Oct', className: 'va-m', name: 'line16Oct' },
        { title: 'Line 16 Nov', className: 'va-m', name: 'line16Nov' },
        { title: 'Line 16 Dec', className: 'va-m', name: 'line16Dec' },

        { title: 'Line 17 First Name', className: 'va-m', name: 'line17FirstName' },
        { title: 'Line 17 Last Name', className: 'va-m', name: 'line17LastName' },
        { title: 'Line 17 SSN', className: 'va-m', name: 'line17SSN' },
        { title: 'Line 17 DateOfBirth', className: 'va-m', name: 'line17DateOfBirth' },
        { title: 'Line 17 All Months', className: 'va-m', name: 'line17AllMonths' },
        { title: 'Line 17 Jan', className: 'va-m', name: 'line17Jan' },
        { title: 'Line 17 Feb', className: 'va-m', name: 'line17Feb' },
        { title: 'Line 17 Mar', className: 'va-m', name: 'line17Mar' },
        { title: 'Line 17 Apr', className: 'va-m', name: 'line17Apr' },
        { title: 'Line 17 May', className: 'va-m', name: 'line17May' },
        { title: 'Line 17 June', className: 'va-m', name: 'line17June' },
        { title: 'Line 17 July', className: 'va-m', name: 'line17July' },
        { title: 'Line 17 Aug', className: 'va-m', name: 'line17Aug' },
        { title: 'Line 17 Sept', className: 'va-m', name: 'line17Sept' },
        { title: 'Line 17 Oct', className: 'va-m', name: 'line17Oct' },
        { title: 'Line 17 Nov', className: 'va-m', name: 'line17Nov' },
        { title: 'Line 17 Dec', className: 'va-m', name: 'line17Dec' },

        
        { title: 'Line 18 First Name', className: 'va-m', name: 'line18FirstName' },
        { title: 'Line 18 Last Name', className: 'va-m', name: 'line18LastName' },
        { title: 'Line 18 SSN', className: 'va-m', name: 'line18SSN' },
        { title: 'Line 18 DateOfBirth', className: 'va-m', name: 'line18DateOfBirth' },
        { title: 'Line 18 All Months', className: 'va-m', name: 'line18AllMonths' },
        { title: 'Line 18 Jan', className: 'va-m', name: 'line18Jan' },
        { title: 'Line 18 Feb', className: 'va-m', name: 'line18Feb' },
        { title: 'Line 18 Mar', className: 'va-m', name: 'line18Mar' },
        { title: 'Line 18 Apr', className: 'va-m', name: 'line18Apr' },
        { title: 'Line 18 May', className: 'va-m', name: 'line18May' },
        { title: 'Line 18 June', className: 'va-m', name: 'line18June' },
        { title: 'Line 18 July', className: 'va-m', name: 'line18July' },
        { title: 'Line 18 Aug', className: 'va-m', name: 'line18Aug' },
        { title: 'Line 18 Sept', className: 'va-m', name: 'line18Sept' },
        { title: 'Line 18 Oct', className: 'va-m', name: 'line18Oct' },
        { title: 'Line 18 Nov', className: 'va-m', name: 'line18Nov' },
        { title: 'Line 18 Dec', className: 'va-m', name: 'line18Dec' },

        
        { title: 'Line 19 First Name', className: 'va-m', name: 'line19FirstName' },
        { title: 'Line 19 Last Name', className: 'va-m', name: 'line19LastName' },
        { title: 'Line 19 SSN', className: 'va-m', name: 'line19SSN' },
        { title: 'Line 19 DateOfBirth', className: 'va-m', name: 'line19DateOfBirth' },
        { title: 'Line 19 All Months', className: 'va-m', name: 'line19AllMonths' },
        { title: 'Line 19 Jan', className: 'va-m', name: 'line19Jan' },
        { title: 'Line 19 Feb', className: 'va-m', name: 'line19Feb' },
        { title: 'Line 19 Mar', className: 'va-m', name: 'line19Mar' },
        { title: 'Line 19 Apr', className: 'va-m', name: 'line19Apr' },
        { title: 'Line 19 May', className: 'va-m', name: 'line19May' },
        { title: 'Line 19 June', className: 'va-m', name: 'line19June' },
        { title: 'Line 19 July', className: 'va-m', name: 'line19July' },
        { title: 'Line 19 Aug', className: 'va-m', name: 'line19Aug' },
        { title: 'Line 19 Sept', className: 'va-m', name: 'line19Sept' },
        { title: 'Line 19 Oct', className: 'va-m', name: 'line19Oct' },
        { title: 'Line 19 Nov', className: 'va-m', name: 'line19Nov' },
        { title: 'Line 19 Dec', className: 'va-m', name: 'line19Dec' },

        
        { title: 'Line 20 First Name', className: 'va-m', name: 'line20FirstName' },
        { title: 'Line 20 Last Name', className: 'va-m', name: 'line20LastName' },
        { title: 'Line 20 SSN', className: 'va-m', name: 'line20SSN' },
        { title: 'Line 20 DateOfBirth', className: 'va-m', name: 'line20DateOfBirth' },
        { title: 'Line 20 All Months', className: 'va-m', name: 'line20AllMonths' },
        { title: 'Line 20 Jan', className: 'va-m', name: 'line20Jan' },
        { title: 'Line 20 Feb', className: 'va-m', name: 'line20Feb' },
        { title: 'Line 20 Mar', className: 'va-m', name: 'line20Mar' },
        { title: 'Line 20 Apr', className: 'va-m', name: 'line20Apr' },
        { title: 'Line 20 May', className: 'va-m', name: 'line20May' },
        { title: 'Line 20 June', className: 'va-m', name: 'line20June' },
        { title: 'Line 20 July', className: 'va-m', name: 'line20July' },
        { title: 'Line 20 Aug', className: 'va-m', name: 'line20Aug' },
        { title: 'Line 20 Sept', className: 'va-m', name: 'line20Sept' },
        { title: 'Line 20 Oct', className: 'va-m', name: 'line20Oct' },
        { title: 'Line 20 Nov', className: 'va-m', name: 'line20Nov' },
        { title: 'Line 20 Dec', className: 'va-m', name: 'line20Dec' },

        
        { title: 'Line 21 First Name', className: 'va-m', name: 'line21FirstName' },
        { title: 'Line 21 Last Name', className: 'va-m', name: 'line21LastName' },
        { title: 'Line 21 SSN', className: 'va-m', name: 'line21SSN' },
        { title: 'Line 21 DateOfBirth', className: 'va-m', name: 'line21DateOfBirth' },
        { title: 'Line 21 All Months', className: 'va-m', name: 'line21AllMonths' },
        { title: 'Line 21 Jan', className: 'va-m', name: 'line21Jan' },
        { title: 'Line 21 Feb', className: 'va-m', name: 'line21Feb' },
        { title: 'Line 21 Mar', className: 'va-m', name: 'line21Mar' },
        { title: 'Line 21 Apr', className: 'va-m', name: 'line21Apr' },
        { title: 'Line 21 May', className: 'va-m', name: 'line21May' },
        { title: 'Line 21 June', className: 'va-m', name: 'line21June' },
        { title: 'Line 21 July', className: 'va-m', name: 'line21July' },
        { title: 'Line 21 Aug', className: 'va-m', name: 'line21Aug' },
        { title: 'Line 21 Sept', className: 'va-m', name: 'line21Sept' },
        { title: 'Line 21 Oct', className: 'va-m', name: 'line21Oct' },
        { title: 'Line 21 Nov', className: 'va-m', name: 'line21Nov' },
        { title: 'Line 21 Dec', className: 'va-m', name: 'line21Dec' },
    
        { title: 'Line 22 First Name', className: 'va-m', name: 'line22FirstName' },
        { title: 'Line 22 Last Name', className: 'va-m', name: 'line22LastName' },
        { title: 'Line 22 SSN', className: 'va-m', name: 'line22SSN' },
        { title: 'Line 22 DateOfBirth', className: 'va-m', name: 'line22DateOfBirth' },
        { title: 'Line 22 All Months', className: 'va-m', name: 'line22AllMonths' },
        { title: 'Line 22 Jan', className: 'va-m', name: 'line22Jan' },
        { title: 'Line 22 Feb', className: 'va-m', name: 'line22Feb' },
        { title: 'Line 22 Mar', className: 'va-m', name: 'line22Mar' },
        { title: 'Line 22 Apr', className: 'va-m', name: 'line22Apr' },
        { title: 'Line 22 May', className: 'va-m', name: 'line22May' },
        { title: 'Line 22 June', className: 'va-m', name: 'line22June' },
        { title: 'Line 22 July', className: 'va-m', name: 'line22July' },
        { title: 'Line 22 Aug', className: 'va-m', name: 'line22Aug' },
        { title: 'Line 22 Sept', className: 'va-m', name: 'line22Sept' },
        { title: 'Line 22 Oct', className: 'va-m', name: 'line22Oct' },
        { title: 'Line 22 Nov', className: 'va-m', name: 'line22Nov' },
        { title: 'Line 22 Dec', className: 'va-m', name: 'line22Dec' }
    ];

    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table', 'table-striped', 'table-bordered', 'table-hover']
    };

    empDetails: Array<any> = [];
    errorMessage: string;
    constructor(private _original1095cService: Original1095cService) { }

    ngOnInit(): void {
        this.myOptionsYear = [];
        this.myOptionsCG = [];
        this._original1095cService.getReportReferenceData().subscribe(data => {

            data.TaxYears.forEach(element => {
                this.myOptionsYear.push({ id: element, name: element })
            });

            data.ControlGroup.forEach(element => {
                this.myOptionsCG.push({ id: element, name: element })
            });
        },
            error => this.errorMessage = <any>error);
    }


    reset(): void {
        this.selectedYear = [];
        this.selectedControlGroup = [];
        this.dataLoaded = false;
    }

    getFilterValues(): any {

        let tyear = this.selectedYear;
        if (tyear === undefined) {
            tyear = [];
            tyear[0] = '\'\'';
            this.selectedYear = [];
        }
        if (tyear !== undefined && tyear.length <= 0) {
            tyear[0] = '\'\'';
            this.selectedYear = [];
        }

        let cg = this.selectedControlGroup;
        if (cg === undefined) {
            cg = [];
            cg[0] = '\'\'';
            this.selectedControlGroup = [];
        }
        if (cg !== undefined && cg.length <= 0) {
            cg[0] = '\'\'';
            this.selectedControlGroup = [];
        }
        let filterCriteria: any = {
            selectedYear: tyear.join(':'),
            selectedControlGroup: cg.join(':')
        };

        return filterCriteria;
    }

    legalEntitiesReportsData(): void {
        
        const filterCriteria = this.getFilterValues();
        this._original1095cService.getOriginal1095CReportData(filterCriteria).subscribe(empdetails => {
            
            this.empDetails = empdetails;
            this.onChangeTable(this.config);
            this.dataLoaded = true;
        },
            error => this.errorMessage = <any>error);

    }

    Search(): void {
        this.dataLoaded = false;
        this.legalEntitiesReportsData();
    }

    downloadPdf(): void {
    }

    downloadExcel(): void {
        const filterCriteria = this.getFilterValues();
        this._original1095cService.downloadExcelReport(filterCriteria);
    }
    public changeSort(data: any, config: any): any {
        if (!config.sorting) {
            return data;
        }

        const columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '' && columns[i].sort !== false) {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
            Object.assign(this.config.sorting, config.sorting);
        }

        const filteredData = this.changeFilter(this.empDetails, this.config);
        const sortedData = this.changeSort(filteredData, this.config);
        this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }

    public changeFilter(data: any, config: any): any {
        let filteredData: Array<any> = data;
        this.columns.forEach((column: any) => {
            if (column.filtering) {
                filteredData = filteredData.filter((item: any) => {
                    return item[column.name].match(column.filtering.filterString);
                });
            }
        });

        if (!config.filtering) {
            return filteredData;
        }

        if (config.filtering.columnName) {
            return filteredData.filter((item: any) =>
                item[config.filtering.columnName].match(this.config.filtering.filterString));
        }

        const tempArray: Array<any> = [];
        filteredData.forEach((item: any) => {
            let flag = false;
            this.columns.forEach((column: any) => {
                if (item[column.name].toString().match(this.config.filtering.filterString)) {
                    flag = true;
                }
            });
            if (flag) {
                tempArray.push(item);
            }
        });
        filteredData = tempArray;

        return filteredData;
    }

    public changePage(page: any, data: Array<any> = this.empDetails): Array<any> {
        const start = (page.page - 1) * page.itemsPerPage;
        const end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

}
