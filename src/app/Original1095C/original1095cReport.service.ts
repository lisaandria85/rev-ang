import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { IOriginal1095cWorkDetail } from './original1095cWorkDetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';


@Injectable()
export class Original1095cService {
    private _original1095cReportUrl = CONFIGURATION.baseServiceUrl + 'original1095service/';
    constructor(private _http: Http) { }
    getReportReferenceData(): Observable<any> {
        return this._http.get(this._original1095cReportUrl + 'getoriginal1095reportreferencedata')
            .map((response: Response) => response.json().original1095ReportReferenceDataVO)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getOriginal1095CReportData(filterCriteria: any): Observable<IOriginal1095cWorkDetail[]> {
        let fileName = 'getoriginal1095reportdata?TaxYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
            return this._http.get(this._original1095cReportUrl + fileName)
            .map((response: Response) => <IOriginal1095cWorkDetail[]>response.json().original1095ReportData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }
    downloadExcelReport(filterCriteria: any): void {
        let fileName = 'processoriginal1095reportexcelupload?TaxYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
        window.open(this._original1095cReportUrl + fileName, '_bank');
    }
    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }
}
