import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { IAcaDataWorkDetail } from './acaDataWorkdetail';
import { Observable } from 'rxjs/Observable';
import { CONFIGURATION } from '../app.config';


@Injectable()
export class AcaDataReportService {
    private _acaDataReportUrl = CONFIGURATION.baseServiceUrl + 'acadatasetservice/';
    constructor(private _http: Http) { }
    getReportReferenceData(): Observable<any> {
        return this._http.get(this._acaDataReportUrl + 'getacadatasetservicereferencedata')
            .map((response: Response) => response.json().acaDataSetReferenceDataVO)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getAcaDataReports(filterCriteria: any): Observable<IAcaDataWorkDetail[]> {
        let fileName = 'geacadatasetservicereportdata?ControlGroup=' + filterCriteria.selectedControlGroup
            + '&EmployerName=' + filterCriteria.selectedEmployerName
            + '&SourceCode=' + filterCriteria.selectedSourceCode
            + '&EmployeeName=' + filterCriteria.employeeName
            + '&HoursPaid=' + filterCriteria.employeeHoursPaid;
        return this._http.get(this._acaDataReportUrl + fileName)
            .map((response: Response) => <IAcaDataWorkDetail[]>response.json().acaDataSetServiceReportDataVO)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    downloadExcelReport(filterCriteria: any): void {
        let fileName = 'processacadatasetserviceexcelupload?ControlGroup=' + filterCriteria.selectedControlGroup
            + '&EmployerName=' + filterCriteria.selectedEmployerName
            + '&SourceCode=' + filterCriteria.selectedSourceCode
            + '&EmployeeName=' + filterCriteria.employeeName
            + '&HoursPaid=' + filterCriteria.employeeHoursPaid;

        window.open(this._acaDataReportUrl + fileName, '_bank');
    }
    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }
}
