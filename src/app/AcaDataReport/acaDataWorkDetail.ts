export interface IAcaDataWorkDetail {
    controlGroupName: string;
    employerName: string;
    employerFederalTaxID: string;
    ssn: string;
    employeeName: string;
    employeeLastName: string;
    employeeFirstName: string;
    sourceCode: string;
    hoursPaid: string;
    firstPayPeriodStartDate: String;
    employmentStatus: string;
    acaEmploymentStatus: string;
    benefitsEnrollmentStatus: string;
    unionStatus: string;
    hrsFilename: string;
    insFilename: string;
}
