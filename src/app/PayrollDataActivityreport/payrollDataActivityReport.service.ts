import { Injectable } from '@angular/core';
import { Http, Response,Headers,ResponseContentType } from '@angular/http';
import { IWorkDetails } from './workdetail';
import { Observable } from 'rxjs/Observable';

import { CONFIGURATION } from '../app.config';



@Injectable()
export class PayrollDataActivityReportService {
    private _pdaReportUrl = CONFIGURATION.baseServiceUrl + 'payrolldataactivityreportservice/';
    constructor(private _http: Http) {

    }

    getReportData(): Observable<any> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        return this._http.get(this._pdaReportUrl + 'getPayrollDataActivityReportReferenceData',{headers:headers})
            .map((response: Response) => response.json().payrollRefDataVO)
            //.do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPayrollDataActivityReportData(filterCriteria: any): Observable<IWorkDetails[]> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName = 'getPayrollDataActivityReportData?WorkYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;
        return this._http.get(this._pdaReportUrl + fileName,{headers:headers})
            .map((response: Response) => <IWorkDetails[]>response.json().reportsForPayrollDataActivity)
           // .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    downloadPDFReport(filterCriteria: any): any {
        let fileName = 'processPayrollDataActivityReportPDFUpload?WorkYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;

        //window.open(this._pdaReportUrl + fileName, '_bank');
        const url = this._pdaReportUrl + fileName
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        // headers.append('Authorization', 'JWT ' + localStorage.getItem('id_token'));
       return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
            (res) => {
                const a = document.createElement('a');
                document.body.appendChild(a);
                const blob = new Blob([res.blob()], { type: 'application/zip' });
                const downloadUrl = URL.createObjectURL(blob);
                window.open(downloadUrl);
            })
    
    }
    downloadExcelReport(filterCriteria: any): any {
        let fileName = 'processPayrollDataActivityReportExcelUpload?WorkYear=' + filterCriteria.selectedYear
            + '&ControlGroup=' + filterCriteria.selectedControlGroup;

       // window.open(this._pdaReportUrl + fileName, '_bank');
       const url = this._pdaReportUrl + fileName
       const headers = new Headers();
       headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
       headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
       return this._http.get(url, { headers: headers, responseType: ResponseContentType.Blob }).map(
           (res) => {
               const a = document.createElement('a');
               document.body.appendChild(a);
               const blob = new Blob([res.blob()], { type: 'application/zip' });
               const downloadUrl = URL.createObjectURL(blob);
               window.open(downloadUrl);
           })

    }
    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

}
