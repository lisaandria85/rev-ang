import { Component, OnInit } from '@angular/core';
import { FileerrorsService } from './fileerrors.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-filerrors',
  templateUrl: './fileerrors.component.html',
  styleUrls: ['./fileerrors.component.css']
})
export class FileerrorsComponent implements OnInit {
  errId: number;
  constructor(private FileerrorsService: FileerrorsService, private route: ActivatedRoute) { }

  fileErrors = {};

  ngOnInit() {
    this.errId = parseInt(this.route.snapshot.params['errId']);  
    this.FileerrorsService.getFileErrors(this.errId).subscribe(data => this.fileErrors = data);
  }
}
