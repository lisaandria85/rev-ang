import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';


@Injectable()
export class FileerrorsService {

  constructor(private http: Http) { }
  private _filemannagementUrl = CONFIGURATION.basefileformsURL + 'file/';

  getFileErrors(errId: number) {
    return this.http.get(this._filemannagementUrl + '/errors/' + errId)
      .map((res: Response) => res.json());
  }
}
