import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';

@Injectable()
export class GenerateIRSFileService {
  private _generateIRSUrl = CONFIGURATION.baseGenerateIrsURL;

  constructor(private http: Http) { }

  getGenerateIRSReferenceData() {
    return this.http.get(this._generateIRSUrl + 'referencedata')
      .map((res: Response) => res.json());
  }
  getGenerateIRS(taxYear: string, ein: string, processStatus: string) {
    if('-1'=== taxYear) taxYear='';
    if('-1'=== ein) ein='';
    if('-1'=== processStatus) processStatus='';
    return this.http.get(this._generateIRSUrl + `/${taxYear}/${ein}/109495C/airrequest/${processStatus}`)
      .map((res: Response) => res.json());
  }
  submitForGenerateIRSFile(taxYear: string, ein: string, count: string, processStatus : string) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const content = JSON.stringify(
      {
        taxYear: taxYear,
        ein: ein,
        count1095: count,
        createdBy: 'aca',
        processStatus: processStatus
      }
    );
    return this.http.post(this._generateIRSUrl + 'generatexmlforirssubmission', content,
      {
        headers: headers
      }).map((res: Response) => res.json());
  }
}
