import { TestBed, inject } from '@angular/core/testing';

import { GenerateIRSFileService } from './generate-irsfile.service';

describe('GenerateIRSFileService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenerateIRSFileService]
    });
  });

  it('should be created', inject([GenerateIRSFileService], (service: GenerateIRSFileService) => {
    expect(service).toBeTruthy();
  }));
});
