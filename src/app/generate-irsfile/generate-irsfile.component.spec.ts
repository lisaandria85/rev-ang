import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateIRSFileComponent } from './generate-irsfile.component';

describe('GenerateIRSFileComponent', () => {
  let component: GenerateIRSFileComponent;
  let fixture: ComponentFixture<GenerateIRSFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateIRSFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateIRSFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
