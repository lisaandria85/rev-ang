import { Component, OnInit } from '@angular/core';
import { GenerateIRSFileService } from './generate-irsfile.service';

import { AlertService } from '../alert/index';

import * as _ from 'lodash';
import { file1094Service } from '../file1094/file1094.service';

@Component({
  selector: 'app-generate-irsfile',
  templateUrl: './generate-irsfile.component.html',
  providers: [GenerateIRSFileService, file1094Service],
  styleUrls: ['./generate-irsfile.component.css']
})
export class GenerateIRSFileComponent implements OnInit {

  public einList: any[];
  public taxYearList: any[];
  dataLoaded: boolean;
  generateIRSList: any = [];
  public loading = false;

  // TODO - what is the use of this
  controlGroupList: any;

  public selectedYear = '-1';
  public selectedEIN = '-1';
  public selectedGroup = '-1';
  public selectedProcessStatus = '-1';

  constructor(private service: GenerateIRSFileService, private file1094Serv: file1094Service, private alertService: AlertService) { }

  ngOnInit() {
    // this.service.getGenerateIRSReferenceData().subscribe(data => {
    //   this.einList = data.einList;
    //   this.taxYearList = data.taxYearList;
    // });
    this.file1094Serv.get1094yearlist().subscribe(data => {
      this.taxYearList = data.taxYears;
    });
  }

  ontaxYearChange() {
    if (this.selectedYear) {
      this.file1094Serv.get1094controlgroup(this.selectedYear).subscribe(data => {
        this.controlGroupList = data.controlGroups;
      });
    }
  }

  onprocessstatusChange() {
    if (this.selectedProcessStatus) {
      this.file1094Serv.getIRSeinlist(this.selectedYear, this.selectedGroup, this.selectedProcessStatus).subscribe(data => {
        if (data.einList.length > 0) {
          this.einList = ['ALL'];
          this.einList.push(data.einList);
        }
        else {
          this.einList = [];
        }
      });
    }
  }

  reset(): void {
    this.loading = false;
    this.dataLoaded = false;
    this.selectedYear = '-1';
    this.selectedEIN = '-1';
    this.selectedGroup = '-1';
    this.selectedProcessStatus = '-1';
    this.alertService.clear();
    // this.getListOfFiles()
  }
  getAllGenerateIRS() {
    this.alertService.clear();
    this.loading = true;
    if (this.selectedYear == '-1' || this.selectedProcessStatus == '-1' || this.selectedEIN == '-1' || this.selectedProcessStatus == '-1') {
      this.alertService.error('Please select all required fields');
      this.loading = false;
      return;
    }
    this.service.getGenerateIRS(this.selectedYear, this.selectedEIN, this.selectedProcessStatus).subscribe(data => {
      this.generateIRSList = data;
      this.dataLoaded = true;
      this.loading = false;
    },
      (error) => this.loading = false);
  }
  canGenerateIRSFile(): boolean {
    if (_.has(this.generateIRSList, 'count1095')) {
      if (this.generateIRSList.count1095 > 0)
        return true;
    }
    return false;
  }
  submitForGenerateIRSFile(year, ein, count, status) {
    this.service.submitForGenerateIRSFile(year, ein, count, status).subscribe(data => {
      this.generateIRSList = data;
    });
  }
}
