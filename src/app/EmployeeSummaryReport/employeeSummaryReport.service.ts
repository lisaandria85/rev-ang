import { Injectable } from '@angular/core';
import { Http, Response,Headers } from '@angular/http';
import { IEmployeeSummaryDetail } from './employeeSummaryDetail';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class EmployeeSummaryReportService {
    private _employeeSummaryReportUrl = 'app/api/';

    constructor(private _http: Http) { }
    getEmployeeSummaryReports(): Observable<IEmployeeSummaryDetail[]> {
        const headers = new Headers();
        headers.append('Client-Id', 'REPORTING-PORTAL-API-CLIENT');
        headers.append('Client-Secret', '0667b514-28a8-4928-9aa4-1ffe290ebcd2');
        let fileName = 'employeeSummary.json';
        return this._http.get(this._employeeSummaryReportUrl + fileName,{headers:headers})
            .map((response: Response) => <IEmployeeSummaryDetail[]>response.json())
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Failed in web api(Server error) ');
    }
}
