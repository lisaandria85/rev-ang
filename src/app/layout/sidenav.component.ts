import { Component } from '@angular/core';
import { CONFIGURATION } from '../app.config';

@Component({
    moduleId: module.id,
    selector: 'app-sidenav',
    templateUrl: 'sidenav.html'
})
export class SideNavComponent {
    route: string = CONFIGURATION.reportingroute;
    dashboard: string = CONFIGURATION.dashboard;
    nftreport: string = CONFIGURATION.nftreport;
    enftreport: string = CONFIGURATION.enftreport;
    ogreport: string = CONFIGURATION.ogreport;
    pdareport: string = CONFIGURATION.pdareport;
    ercreport: string = CONFIGURATION.ercreport;
    empsummary: string = CONFIGURATION.empsummary;
    empeligilbility: string = CONFIGURATION.empeligilbility;
    empstabilityperiod: string = CONFIGURATION.empstabilityperiod;
    empdemographics: string = CONFIGURATION.empdemographics;
    empbreakinservice: string = CONFIGURATION.empbreakinservice;
    addcustomer: string = CONFIGURATION.addcustomer;
    listcustomer: string = CONFIGURATION.listcustomer;
    onboardingcustomerinformation: string = CONFIGURATION.onboardingcustomerinformation;
    clientpayroll: string = CONFIGURATION.clientpayroll;
    onboardingpersonalinformation: string = CONFIGURATION.onboardingpersonalinformation;
    uploaddata: string = CONFIGURATION.uploaddata;
    aledataupload: string = CONFIGURATION.aledataupload;
    payrolldataupload: string = CONFIGURATION.payrolldataupload;
    filemanager: string = CONFIGURATION.filemanager;
    usermanagement: string = CONFIGURATION.usermanagement;

    file1094: string = CONFIGURATION.file1094;
    file1095: string = CONFIGURATION.file1095;
    irsfilemanagement: string = CONFIGURATION.irsfilemanagement;
    generateirs: string = CONFIGURATION.generateirs;

    insurancedataupload: string = CONFIGURATION.insurancedataupload;
    onezeroninefourdataupload: string = CONFIGURATION.onezeroninefourdataupload;
    onezeroninefivedataupload: string = CONFIGURATION.onezeroninefivedataupload;
    controlgroup: string = CONFIGURATION.controlgroup;
    ale: string = CONFIGURATION.ale;
    aledetails: string = CONFIGURATION.aledetails;

    legalentities: string = CONFIGURATION.legalentities;
    insuranceinfo: string = CONFIGURATION.insuranceinfo;
    acadata: string = CONFIGURATION.acadata;
    o1095c: string = CONFIGURATION.o1095c;
    shownames: string = CONFIGURATION.shownames;
    payhistory:string=CONFIGURATION.payhistory;

}
