import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { File1095Component } from './file1095.component';

describe('File1095Component', () => {
  let component: File1095Component;
  let fixture: ComponentFixture<File1095Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ File1095Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(File1095Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
