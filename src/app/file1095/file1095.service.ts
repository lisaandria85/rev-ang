import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { CONFIGURATION } from '../app.config';

import * as _ from 'lodash';

@Injectable()
export class file1095Service {
    private _filemannagementUrl = CONFIGURATION.basefile1094CURL;

    constructor(private http: Http) { }

    // Get Tax Yers List
    get1095ReferenceData() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        return this.http.get(this._filemannagementUrl + '1095c/tax-years', {headers: headers})
            .map((res: Response) => res.json());
    }

    // Get Control Grouop list
    get1095controlgroup(value: any) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        return this.http.get(this._filemannagementUrl + '1095c/control-groups?taxYear=' + value, {headers: headers})
        .map((res: Response) => res.json());
    }

    // Get EIN & Source ID
     get1905ein(value: any) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
         return this.http.get(this._filemannagementUrl + '1095c/ein-list?controlGroupName=' + value, {headers: headers})
         .map((res: Response) => res.json());
     }

     // Get SSN List
     get1095SSNlist(value: any) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        return this.http.get(this._filemannagementUrl + '1095c/ssn-and-source?ein=' + value, {headers: headers})
        .map((res: Response) => res.json());
    }

    // Get All records
    getAll1095BySearchCriteria(taxYear: number, ein: string, controlGroup: string, ssn: string) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json;charset=UTF-8');
        headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
        headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
        if ('-1' === ein) ein = '';
         if('-1'=== controlGroup) controlGroup='';
        if('-1'=== ssn) ssn='';
        return this.http.get(this._filemannagementUrl +
        `1095c/getAll1095BySearchCriteria?taxYear=${taxYear}&ein=${ein}&ssn=${ssn}&controlGroup=${controlGroup}`, {headers: headers})
            .map((res: Response) => res.json());
    }

    // Delete EIN
    delete1095(selectList: any[]) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
      headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
      const mapped1095 = _.map(selectList, (item) => {
        const requestObject: any = {};
        requestObject.dataRecord1095Id = item.dataRecord1095Id;
        return requestObject;
      });
      const content = JSON.stringify(mapped1095);
      return this.http.post(this._filemannagementUrl +  '1095c/delete', content, {headers: headers})
        .map((res: Response) => res.json());
    }


    // Approve EIN
    approve1095(selectList: any[]) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json;charset=UTF-8');
      headers.append('Client-Id', 'HIC-REPORTING-API-CLIENT');
      headers.append('Client-Secret', 'f65ce994-e74e-11e7-80c1-9a214cf093ae');
      const mapped1095 = _.map(selectList, (item) => {
        const requestObject: any = {};
        requestObject.dataRecord1095Id = item.dataRecord1095Id;
        return requestObject;
      });
      const content = JSON.stringify(mapped1095);
      return this.http.post(this._filemannagementUrl + '1095c/approve', content, {headers: headers})
        .map((res: Response) => res.json());
    }
}
