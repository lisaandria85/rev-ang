import { Component, OnInit } from '@angular/core';
import { file1095Service } from './file1095.service';
import { SharedService } from '../app.shared.service';
import { Router } from '@angular/router';

import { AlertService } from '../alert/index';

import * as _ from 'lodash';

@Component({
  selector: 'app-file1095',
  templateUrl: './file1095.component.html',
  styleUrls: ['./file1095.component.css'],
  providers: [file1095Service],
})
export class File1095Component implements OnInit {
  control: Response;
  taxyear: any;

  public einList: any[];
  public taxYearList: any[];
  public file1095List: any[];
  public controlGroupList: any;
  public ssnList: any[];
  public sourceids: any[];
  dataLoaded: boolean;

  public selectedYear: number = -1;
  public selectedEIN: string = '-1';
  public selectedSSN: string= '-1';
  public selectedGroup: string= '-1';
  public selectedSource: string= '-1';


  public loading = false;
  public approve1095Progress = false;
  public delete1095Progress = false;
  constructor(private file1095Serv: file1095Service,
    private sharedService: SharedService,
    private router: Router, private alertService: AlertService) { }

  ngOnInit() {
    this.file1095Serv.get1095ReferenceData().subscribe(data => {
      this.taxYearList = data.taxYears;
    });
  }
  onChangeOftaxyear() {
    if (this.selectedYear) {
      this.file1095Serv.get1095controlgroup(this.selectedYear).subscribe(data  =>  {
      this.controlGroupList = data.controlGroups;
      });
    }
  }
  onChangeofcontrolgroup() {
    if (this.selectedGroup) {
      this.file1095Serv.get1905ein(this.selectedGroup).subscribe(data => {
        this.einList = data.einList;
      })
    }
  }
  onChangeofEINlist() {
    if (this.selectedEIN) {
      this.file1095Serv.get1095SSNlist(this.selectedEIN).subscribe(data => {
        this.ssnList = data.ssnList;
        this.sourceids = data.sourceIds;
      })
    }
  }

  reset(): void {
    this.alertService.clear();
    this.dataLoaded = false;
    this.selectedYear = -1;
    this.selectedEIN = '-1';
    this.selectedGroup = '-1';
    this.selectedSSN = '-1';
    this.selectedSource = '-1';
    // this.getListOfFiles()
  }
  checkAll(ev) {
    this.file1095List.forEach(x => x.checked = ev.target.checked)
  }
  isAllChecked() {
    return this.file1095List.every(_ => _.checked);
  }

  delete1095() {
    this.delete1095Progress = true;
    const selected1095s = _.filter(this.file1095List, {checked: true});
    this.file1095Serv.delete1095(selected1095s).subscribe(data => {
      console.log('Success', data.toString());
      this.getAll1095BySearchCriteria();
    });
    this.delete1095Progress = false;
  }
  approve1095() {
    this.approve1095Progress = true;
    const selected1095s = _.filter(this.file1095List, {checked: true});
    this.file1095Serv.approve1095(selected1095s).subscribe(data => {
      console.log('Success', data.toString());
      this.getAll1095BySearchCriteria();
    });
    this.approve1095Progress = false;
  }
  getAll1095BySearchCriteria() {
    this.loading = true;
    this.alertService.clear();
    if (this.selectedYear == -1 ||  this.selectedGroup == '-1') {
      this.alertService.error('Please select year, Control Group');
      this.loading = false;
      return;
    }
    this.file1095Serv.getAll1095BySearchCriteria(this.selectedYear, this.selectedEIN, this.selectedGroup, this.selectedSSN).subscribe(
      data => {
        this.alertService.clear();
        this.file1095List = _.map(data, function(element) {
          return _.extend({}, element, {checked: false});
        });
        this.dataLoaded = true;
        this.loading = false;
        console.log(this.file1095List);
    });
  }

  shareObj(data: any) {
    this.sharedService.setObj(data);
    this.router.navigate(['/reporting/details1095']);
  }
}
