entry: {
  'app': './src/main.ts'
},
module.exports = {
  plugins: [
    new webpack.optimize.UglifyJsPlugin()
  ]
}
